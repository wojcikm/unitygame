﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlashLight : MonoBehaviour {

    public float currentEnergy;
    public bool flashLightOn;
    private bool flashing;
    private float energyUsage;
    private Light latarka;
      


    // Use this for initialization
    void Start () {
        latarka = this.GetComponent<Light>();
        latarka.intensity = 0;
        currentEnergy = 100;
        latarka.enabled = false;
        flashLightOn = false;
        energyUsage = 2;
        flashing = false;

	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.G))
        {
            if (flashLightOn)
            {
                latarka.enabled = false;
                
            }
            else
            {
                latarka.enabled = true;
                
            }
            flashLightOn = !flashLightOn;
        }

        if (flashLightOn && !flashing) {
            currentEnergy -= energyUsage * Time.deltaTime;
            if (currentEnergy <= 0)
            {
                latarka.enabled = false;
                flashLightOn = false;
            }

            if (currentEnergy > 50)
            {
                latarka.intensity = 1;
            }
            else if (currentEnergy <= 50 && currentEnergy >= 20)
            {
                latarka.intensity = 1 / (50 / currentEnergy);
            }
            else if(currentEnergy < 20)
            {
                latarka.intensity = 1 / (50 / currentEnergy);
                if (currentEnergy < 19.5 && currentEnergy > 19.0)
                    latarka.intensity = 0;
                if (currentEnergy < 13.5 && currentEnergy > 12.5)
                    latarka.intensity = 0;
                if (currentEnergy < 11.5 && currentEnergy > 11.0)
                    latarka.intensity = 0;
            }
        }
	}

    void ChangeBattery()
    {
        currentEnergy = 100.0f;
    }

    
}
