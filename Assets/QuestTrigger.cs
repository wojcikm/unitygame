﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestTrigger : MonoBehaviour {

    public int ObjectiveID;
    private List<Quest> questLog;
    private ObjectiveController objective;

    // Use this for initialization
    void Start () {
        questLog = GameObject.Find("Player").GetComponent<QuestLog>().QuestsLog;
        objective = GameObject.Find("ObjectiveDatabase").GetComponent<ObjectiveController>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            objective.UpdateGoProgress(ObjectiveID);
            
        }
    }
}
