﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Answer {

    public int AnswerID; //ID of an Answer
    public string AnswerText; //Text of an answer
    public List<string> AnswerEffects; //list of effects after clicking on answer

    public Answer(int answerID, string answerText, List<string> answerEffects)
    {
        this.AnswerID = answerID;
        this.AnswerText = answerText;
        this.AnswerEffects = answerEffects;
    }


	
}
