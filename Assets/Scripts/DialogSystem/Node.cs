﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Node {

    public int NodeID; //id of current conversation node
    public string NodeText; //text of the converstation node
    public List<Answer> NodeAnswers; //List of answers in current converstaion node

    public Node(int nodeID, string nodeText, List<Answer> nodeAnswers)
    {
        this.NodeID = nodeID;
        this.NodeText = nodeText;
        this.NodeAnswers = nodeAnswers;
    }
}
