﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System;

public class NodeDatabase : MonoBehaviour {

    public List<Node> database;
    private JsonData nodeData;
    // Use this for initialization
    void Awake() {
        nodeData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Nodes.json"));
        ConstructNodeDatabase();
    }

    private void ConstructNodeDatabase()
    {
        for (int i = 0; i < nodeData.Count; i++)
        {
            List<Answer> answersList = new List<Answer>();
            for (int j = 0; j < nodeData[i]["answers"].Count; j++)
            {
                int answerID = (int)nodeData[i]["answers"][j]["answerID"];
                string answerText = (string)nodeData[i]["answers"][j]["answerText"];
                List<string> effectsList = new List<string>();
                for (int k = 0; k < nodeData[i]["answers"][j]["effects"].Count; k++)
                {
                    effectsList.Add((string)nodeData[i]["answers"][j]["effects"][k]);
                }
                
                Answer newAnswer = new Answer(answerID,answerText,effectsList);
                answersList.Add(newAnswer);
            }


            database.Add(new Node(
                (int)nodeData[i]["nodeID"],
                nodeData[i]["nodeText"].ToString(),
                answersList));
        }
    }
    public Node FetchNodeById(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (id == database[i].NodeID)
                return database[i];
        }

        return null;
    }

    public Answer FetchAnswerById(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            for(int j=0;j<database[i].NodeAnswers.Count;j++)
            {
                if (id == database[i].NodeAnswers[j].AnswerID)
                    return database[i].NodeAnswers[j];
            }
            
        }

        return null;
    }
}

    
