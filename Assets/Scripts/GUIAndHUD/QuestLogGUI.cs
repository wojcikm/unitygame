﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class QuestLogGUI : MonoBehaviour {

    public GameObject QuestButtonPrefab;
    public GameObject ObjectiveButtonPrefab;
    public GameObject RewardButtonPrefab;

    private QuestLog questLog;
    private ItemDatabase itemDatabase;
    private MonsterDatabase monsterDatabase;

    private Vector3 questFirstPosition = new Vector3(-165, 78, 0);
    private Vector3 objectiveFirstPosition = new Vector3(0, 36f, 0);
    private Quaternion questFirstRotation = new Quaternion();

    public GameObject questLogGUICanvas;
    public GameObject objectiveScrollViewCanvas;
    public GameObject rewardsScrollViewCanvas;

    public List<GameObject> Quests;
    public List<GameObject> Objectives;
    public List<GameObject> Rewards;

    private GameObject lastButton;
    private GameObject thisButton;

    public GameObject WindowObject;

    public Sprite[] ObjectiveTypes;
    public Sprite GoldIcon;
    public Sprite ExperienceIcon;

    // Use this for initialization
    void Start()
    {
        questLog = GameObject.Find("Player").GetComponent<QuestLog>();
        itemDatabase = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();
        monsterDatabase = GameObject.Find("MonsterDatabase").GetComponent<MonsterDatabase>();
    }

    public void OpenQuestLog()
    {
        if (WindowObject.active == false)
        {
            WindowObject.SetActive(true);
                      
            for (int i = 0; i < questLog.QuestsLog.Count; i++)
            {
                Quests.Add(Instantiate(QuestButtonPrefab, questFirstPosition + new Vector3(0, -i * 30, 0), questFirstRotation) as GameObject);
                Quests[i].transform.SetParent(questLogGUICanvas.transform, false);
                Quests[i].GetComponentInChildren<Text>().text = questLog.QuestsLog[i].Name;
                Quests[i].GetComponent<Button>().onClick.AddListener(() => { ActiveQuestButton(); });
                Quests[i].GetComponent<Button>().onClick.AddListener(() => { SetQuestName(); });
                
            }
            Quests[0].GetComponent<Button>().image.color = Color.black;
            thisButton = Quests[0];
            SetQuestName();
            gameObject.transform.parent.GetComponent<GUIHierarchyController>().SetOnTopOfHierarchy(gameObject);
        }
        else
        {
            ClearQuestsList();
            ClearQuestName();
            WindowObject.SetActive(false);
        }
    }    
    private void DisplayActiveQuests()
    {
        int counter = 0;
        ClearQuestsList();
        ClearQuestName();
        questLog = GameObject.Find("Player").GetComponent<QuestLog>();
        for (int i = 0; i < questLog.QuestsLog.Count; i++)
        {
            if (!questLog.QuestsLog[i].IsReturned)
            {
                counter++;
                Quests.Add(Instantiate(QuestButtonPrefab, questFirstPosition + new Vector3(0, -i * 30, 0), questFirstRotation) as GameObject);
                Quests[counter].transform.SetParent(questLogGUICanvas.transform, false);
                Quests[counter].GetComponentInChildren<Text>().text = questLog.QuestsLog[i].Name;
                Quests[counter].GetComponent<Button>().onClick.AddListener(() => { ActiveQuestButton(); });
                Quests[counter].GetComponent<Button>().onClick.AddListener(() => { SetQuestName(); });
                


            }
        }

        if (Quests.Count > 0)
        {
            SelectFirstQuestOnList();
            SetQuestName();
        }

    }
    private void ActiveQuestButton()
    {        
        lastButton = thisButton;
        thisButton = EventSystem.current.currentSelectedGameObject;
        if (lastButton != thisButton)
        {
            Button tB = thisButton.GetComponent<Button>();
            tB.image.color = Color.black;
            if (lastButton)
            {
                Button lB = lastButton.GetComponent<Button>();
                lB.image.color = Color.white;
            }
        }
    }
    private void DisplayCompletedQuests()
    {
        ClearQuestsList();
        ClearQuestName();
        questLog = GameObject.Find("Player").GetComponent<QuestLog>();
        for (int i = 0; i < questLog.QuestsLog.Count; i++)
        {
            if (questLog.QuestsLog[i].IsReturned)
            {
                Quests.Add(Instantiate(QuestButtonPrefab, questFirstPosition + new Vector3(0, -i * 30, 0), questFirstRotation) as GameObject);
                Quests[i].transform.SetParent(questLogGUICanvas.transform, false);
                Quests[i].GetComponentInChildren<Text>().text = questLog.QuestsLog[i].Name;
                
            }
        }
        if (Quests.Count > 0)
        {
            SelectFirstQuestOnList();
            SetQuestName();
        }
    }
    private void ClearQuestsList()
    {
        for(int i=0;i<Quests.Count;i++)
        {
            Destroy(Quests[i]);
        }
        Quests.Clear();
    }
    private void ClearObjectivesList()
    {
        for (int i = 0; i < Objectives.Count; i++)
        {
            Destroy(Objectives[i]);
        }
        Objectives.Clear();
    }
    private void ClearRewardsList()
    {
        for (int i = 0; i < Rewards.Count; i++)
        {
            Destroy(Rewards[i]);
        }
        Rewards.Clear();
    }
    public void ClearQuestName()
    {
        GameObject.Find("QuestNameText").GetComponent<Text>().text = "";
        GameObject.Find("QuestDescriptionText").GetComponent<Text>().text = "";
        ClearObjectivesList();
        ClearRewardsList();
    }
    public void SelectFirstQuestOnList()
    {             
        if (Quests[0])
        {
            thisButton = Quests[0];
            Button tB = thisButton.GetComponent<Button>();
            tB.image.color = Color.black;
        }


    }
    
    
    public void SetQuestName()
    {
        Quest clickedQuest = questLog.FetchQuestByName(thisButton.GetComponentInChildren<Text>().text);
        GameObject.Find("QuestNameText").GetComponent<Text>().text = clickedQuest.Name;
        GameObject.Find("QuestDescriptionText").GetComponent<Text>().text = clickedQuest.DescriptionSummary;
        SetObjectiveList(clickedQuest);
        SetRewardsList(clickedQuest);
    }
    private void SetObjectiveList(Quest clickedQuest)
    {
        ClearObjectivesList();
        string type = "";
        string amount = "";
        string target = "";
        for (int i = 0; i < clickedQuest.Objectives.Count; i++)
        {
            Objectives.Add(Instantiate(ObjectiveButtonPrefab, objectiveFirstPosition, ObjectiveButtonPrefab.transform.rotation) as GameObject);
            Objectives[i].transform.SetParent(objectiveScrollViewCanvas.transform, false);
            if (clickedQuest.Objectives[i].Type == Objective.ObjectiveType.Talk)
            {
                Objectives[i].GetComponentInChildren<Image>().sprite = ObjectiveTypes[0];
                type = "Talk to ";
                target = clickedQuest.Objectives[i].ObjectiveTalk.NodeIdToOpen.ToString();
            }
            else if (clickedQuest.Objectives[i].Type == Objective.ObjectiveType.Collect)
            {
                Objectives[i].GetComponentInChildren<Image>().sprite = ObjectiveTypes[1];
                type = "Collect ";
                amount = clickedQuest.Objectives[i].ObjectiveCollect.CurrentAmountCollected.ToString() + "/" + clickedQuest.Objectives[i].ObjectiveCollect.AmountToCollect.ToString();
                target = itemDatabase.FetchItemById(clickedQuest.Objectives[i].ObjectiveCollect.ItemToCollect).Name;
            }
            else if (clickedQuest.Objectives[i].Type == Objective.ObjectiveType.Go)
            {
                Objectives[i].GetComponentInChildren<Image>().sprite = ObjectiveTypes[2];
                type = "Go to ";
                target = clickedQuest.Objectives[i].ObjectiveGo.PlaceToGo.ToString();
            }
            else if (clickedQuest.Objectives[i].Type == Objective.ObjectiveType.Kill)
            {
                Objectives[i].GetComponentInChildren<Image>().sprite = ObjectiveTypes[3];
                type = "Kill ";
                amount = clickedQuest.Objectives[i].ObjectiveKillMonster.CurrentMonsterKill.ToString() + "/" + clickedQuest.Objectives[i].ObjectiveKillMonster.AmountToKill.ToString();
                target = monsterDatabase.FetchMonsterById(clickedQuest.Objectives[i].ObjectiveKillMonster.MonsterToKill).Name;
            }
            Objectives[i].transform.GetChild(0).GetComponent<Text>().text = type + target + " " + amount;
            if(clickedQuest.Objectives[i].IsComplete)
            {
                Objectives[i].GetComponentInChildren<Image>().color = Color.green;
            }
            else
            {
                Objectives[i].GetComponentInChildren<Image>().color = Color.white;
            }
        }
    }
    private void SetRewardsList(Quest clickedQuest)
    {
        ClearRewardsList();
        string type = "";
        string amount = "";
        int counter = 0;
        if (clickedQuest.RewardExp > 0)
        {
            Rewards.Add(Instantiate(RewardButtonPrefab, objectiveFirstPosition, ObjectiveButtonPrefab.transform.rotation) as GameObject);
            Rewards[counter].transform.SetParent(rewardsScrollViewCanvas.transform, false);
            Rewards[counter].GetComponentInChildren<Image>().sprite = ExperienceIcon;
            type = "Experience ";
            amount = clickedQuest.RewardExp.ToString();
            Rewards[counter].transform.GetChild(0).GetComponent<Text>().text = type + amount;
            counter++;
        }
        if (clickedQuest.RewardGold > 0)
        {
            Rewards.Add(Instantiate(RewardButtonPrefab, objectiveFirstPosition, ObjectiveButtonPrefab.transform.rotation) as GameObject);
            Rewards[counter].transform.SetParent(rewardsScrollViewCanvas.transform, false);
            Rewards[counter].GetComponentInChildren<Image>().sprite = GoldIcon;
            type = "Gold ";
            amount = clickedQuest.RewardGold.ToString();
            Rewards[counter].transform.GetChild(0).GetComponent<Text>().text = type + amount;
            counter++;
        }

        if (clickedQuest.RewardItem != 0)
        {
            Rewards.Add(Instantiate(RewardButtonPrefab, objectiveFirstPosition, ObjectiveButtonPrefab.transform.rotation) as GameObject);
            Rewards[counter].transform.SetParent(rewardsScrollViewCanvas.transform, false);
            Rewards[counter].GetComponentInChildren<Image>().sprite = itemDatabase.FetchItemById(clickedQuest.RewardItem).icon;
            type = itemDatabase.FetchItemById(clickedQuest.RewardItem).Name;
            amount = clickedQuest.RewardItemAmount.ToString();
            Rewards[counter].transform.GetChild(0).GetComponent<Text>().text = type + amount;
            counter++;
        }
        

        
    }
}
