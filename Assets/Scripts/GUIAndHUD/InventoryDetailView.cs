﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InventoryDetailView : MonoBehaviour {

    public GameObject ItemNameField, ItemIconField;
    public GameObject ArmorLayout, WeaponLayout, QuestLayout, ConsumableLayout, MiscLayout;


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SelectActiveLayout(GameObject layout)
    {
        ArmorLayout.SetActive(false);
        WeaponLayout.SetActive(false);
        QuestLayout.SetActive(false);
        ConsumableLayout.SetActive(false);
        MiscLayout.SetActive(false);
        layout.SetActive(true);
    }

    public void SetItemValues(Item item)
    {
        ItemNameField.GetComponent<Text>().text = item.Name;
        ItemIconField.GetComponent<Image>().sprite = item.icon;

        Item.ItemType itemType = item.Type;
        switch(itemType)
        {
            case Item.ItemType.Armor:
                SelectActiveLayout(ArmorLayout);
                ArmorLayout.GetComponent<EquipLayout>().SetValues(item);
                break;

            case Item.ItemType.Weapon:
                SelectActiveLayout(WeaponLayout);
                WeaponLayout.GetComponent<WeaponLayout>().SetValues(item);

                break;

            case Item.ItemType.Consumable:
                SelectActiveLayout(ConsumableLayout);
                ConsumableLayout.GetComponent<ConsumableLayout>().SetValues(item);
                break;

            case Item.ItemType.Quest:
                SelectActiveLayout(QuestLayout);

                QuestLayout.GetComponent<QuestLayout>().SetValues(item);
                break;

            case Item.ItemType.Misc:
                SelectActiveLayout(MiscLayout);

                MiscLayout.GetComponent<MiscLayout>().SetValues(item);
                break;

            default:

                break;
        }
    }
}
