﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsGUI : MonoBehaviour {

    private PlayerStats playerStats;

    public GameObject StatsWindow;
    public Text Str;
    public Text Agi;
    public Text Int;
    public Text Vit;

    public Text StatPoints;
    public GameObject StatisticDisplay;

    // Use this for initialization
    void Start () {
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();

	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void OpenStatWindow()
    {
        if(StatsWindow.active == false)
        {
            StatsWindow.SetActive(true);
            SetValues();
            StatisticDisplay.GetComponent<StatisticsOverall>().DisplayAll();
            gameObject.transform.parent.GetComponent<GUIHierarchyController>().SetOnTopOfHierarchy(gameObject);
        }
        else
        {
            CloseStatWindow();
            StatsWindow.SetActive(false);
        }
    }
    public void CloseStatWindow()
    {
        if (StatsWindow.active == true)
        {            
            StatsWindow.SetActive(false);
        }
    }

    public void SetValues()
    {
        Agi.text = playerStats.BaseAgility.ToString();
        Str.text = playerStats.BaseStrenght.ToString();
        Int.text = playerStats.BaseInteligence.ToString();
        Vit.text = playerStats.BaseVitality.ToString();
        StatPoints.text = "Points left: " + playerStats.StatisticPoints.ToString();
    }


}
