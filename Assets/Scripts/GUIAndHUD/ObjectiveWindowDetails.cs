﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectiveWindowDetails : MonoBehaviour {

    public GameObject ObjectiveName;
    public GameObject ObjectiveDescription;
    public GameObject ObjectiveType;
    public GameObject ObjectiveActual;

    public GameObject ExperienceAmount;
    public GameObject GoldAmount;
    public GameObject ItemName;
    public GameObject ItemAmount;
    public GameObject ItemIcon;

    private ItemDatabase itemDatabase;
    #if UNITY_EDITOR
    // Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void SetObjectiveDetails(Objective objective)
    {
        itemDatabase = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();
        //ObjectiveName.GetComponent<Text>().text = objective.Name;
       // ObjectiveDescription.GetComponent<Text>().text = objective.DescriptionSummary;
        string type = "";
        string actual = "";
        
        if(objective.Type == Objective.ObjectiveType.Collect)
        {
            type = "Collected";
            actual = objective.ObjectiveCollect.CurrentAmountCollected.ToString() + "/" + objective.ObjectiveCollect.AmountToCollect.ToString();
        }
        else if(objective.Type == Objective.ObjectiveType.Kill)
        {
            type = "Killed";
            actual = objective.ObjectiveKillMonster.CurrentMonsterKill.ToString() + "/" + objective.ObjectiveKillMonster.AmountToKill.ToString();
        }
        else if(objective.Type == Objective.ObjectiveType.Talk)
        {
            type = "Talk";
            //actual = objective.EndID.ToString();
        }
        else if(objective.Type == Objective.ObjectiveType.Go)
        {
            type = "Go to";
            actual = objective.ObjectiveGo.PlaceToGo.ToString();
        }
        ObjectiveType.GetComponent<Text>().text = type;
        ObjectiveActual.GetComponent<Text>().text = actual;
        //ExperienceAmount.GetComponent<Text>().text = objective.RewardExp.ToString();
        //ItemName.GetComponent<Text>().text = itemDatabase.FetchItemById(objective.RewardItem).Name;
       // ItemAmount.GetComponent<Text>().text = objective.RewardItemAmount.ToString();
       // ItemIcon.GetComponent<Image>().sprite = itemDatabase.FetchItemById(objective.RewardItem).icon;
    }
#endif
}
