﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EquipmentWindow : MonoBehaviour {

    public GameObject RightHandObject, LeftHandObject, HeadObject, BodyObject, CapeObject, LegsObject;

    public GameObject WindowDetailsPrefab;
    
    public GameObject WeaponLayout, ArmorLayout;

    public GameObject WindowGameObject;

    private GameObject CurrentDetailsWindow = null;

    private PlayerEquip playerEquip;
    private ItemDatabase itemDatabase;

    public void OpenEquipmentWindow()
    {
        if (WindowGameObject.active == true)
        {
            WindowGameObject.SetActive(false);
        }
        else {
            WindowGameObject.SetActive(true);
            playerEquip = GameObject.Find("Player").GetComponent<PlayerEquip>();
            itemDatabase = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();
            SetEquipment();
            gameObject.transform.parent.GetComponent<GUIHierarchyController>().SetOnTopOfHierarchy(gameObject);
        }
    }

    public void ShowItemDetails(GameObject iconClicked)
    {
        if (CurrentDetailsWindow == null)
        {
            GameObject Button = Instantiate(WindowDetailsPrefab, new Vector3(), new Quaternion()) as GameObject;
            Button.transform.SetParent(iconClicked.transform, false);
            CurrentDetailsWindow = Button;
        }
    }

    public void SetEquipment()
    {
        SetDefaultIcons();


        if (playerEquip.Armor > 0)
        {
            BodyObject.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.FetchItemById(playerEquip.Armor).icon;
            BodyObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { playerEquip.UnequipArmor(); });
            BodyObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { SetEquipment(); });
            BodyObject.GetComponent<InventoryListRightClick>().leftClick.AddListener(() => { ShowItemDetails(BodyObject); });
        }
        if (playerEquip.Gloves > 0)
        {
            CapeObject.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.FetchItemById(playerEquip.Gloves).icon;
            CapeObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { playerEquip.UnequipGloves(); });
            CapeObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { SetEquipment(); });
            CapeObject.GetComponent<InventoryListRightClick>().leftClick.AddListener(() => { ShowItemDetails(CapeObject); });
        }
        if (playerEquip.Helm > 0)
        {
            HeadObject.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.FetchItemById(playerEquip.Helm).icon;
            HeadObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { playerEquip.UnequipHelm(); });
            HeadObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { SetEquipment(); });
        }
        if (playerEquip.Shoes > 0)
        {
            LegsObject.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.FetchItemById(playerEquip.Shoes).icon;
            LegsObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { playerEquip.UnequipShoes(); });
            LegsObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { SetEquipment(); });
        }
        if (playerEquip.Weapon > 0)
        {
            RightHandObject.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.FetchItemById(playerEquip.Weapon).icon;
            RightHandObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { playerEquip.UnequipWeapon(); });
            RightHandObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { SetEquipment(); });

            if (itemDatabase.FetchItemById(playerEquip.Weapon).ItemWeapon.Class == WeaponItem.WeaponType.Bow ||
                itemDatabase.FetchItemById(playerEquip.Weapon).ItemWeapon.Class == WeaponItem.WeaponType.Rod ||
                itemDatabase.FetchItemById(playerEquip.Weapon).ItemWeapon.Class == WeaponItem.WeaponType.TwoHanded)
            {
                LeftHandObject.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.FetchItemById(playerEquip.Weapon).icon;
                LeftHandObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { playerEquip.UnequipWeapon(); });
                LeftHandObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { SetEquipment(); });
            }
            else
            {
                LeftHandObject.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.FetchItemById(playerEquip.Shield).icon;
                LeftHandObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { playerEquip.UnequipShield(); });
                LeftHandObject.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { SetEquipment(); });
            }
        }
    }

    public void SetDefaultIcons()
    {
        BodyObject.transform.GetChild(0).GetComponent<Image>().sprite = null;
        CapeObject.transform.GetChild(0).GetComponent<Image>().sprite = null;
        HeadObject.transform.GetChild(0).GetComponent<Image>().sprite = null;
        LegsObject.transform.GetChild(0).GetComponent<Image>().sprite = null;
        RightHandObject.transform.GetChild(0).GetComponent<Image>().sprite = null;
        LeftHandObject.transform.GetChild(0).GetComponent<Image>().sprite = null;
    }
    
}
