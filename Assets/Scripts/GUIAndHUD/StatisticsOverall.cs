﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class StatisticsOverall : MonoBehaviour{

    private PlayerStats playerStats;
    private PlayerEquip playerEquip;

    public GameObject ButtonStr, ButtonVit, ButtonAgi, ButtonInt;

    public GameObject currentLevel, hpBase, hpUp, hpItem, manaBase, manaUp, manaItem, strBase, strSum,
        strItem, agiBase, agiSum, agiItem, intBase, intSum, intItem, vitBase, vitSum, vitItem,
        hpregBase, hpregUp, hpregItem, manaregBase, manaregUp, manaregItem, closedmgBase, closedmgUp, closeDmgItem,
        rangeddmgBase, rangeddmgUp, rangeddmgItem, magicdmgBase, magicdmgUp, magicdmgItem,
        aspdBase, aspdUp, aspdItem, defenceBase, defenceUp, defenceItem, magicdefencebase, magicdefenceUp, magicdefenceItem;

    

    // Use this for initialization
    void Start()
    {
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        playerEquip = GameObject.Find("Player").GetComponent<PlayerEquip>();
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    
    public void DisplayAll()
    {
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        playerEquip = GameObject.Find("Player").GetComponent<PlayerEquip>();
        SetAllUpZero();
        DisplayBaseAtributes();
        DisplayItemAtributes();
        DisplaySumAtributes();
        DisplayBaseStatistics();        
        DisplayItemStatistics();
    }

    public void DisplaySumAtributes()
    {
        
        strSum.GetComponent<Text>().text = playerStats.Strenght.ToString();
        agiSum.GetComponent<Text>().text = playerStats.Agility.ToString();
        intSum.GetComponent<Text>().text = playerStats.Inteligence.ToString();
        vitSum.GetComponent<Text>().text = playerStats.Vitality.ToString();
    }
    private void DisplayBaseAtributes()
    {
        
        strBase.GetComponent<Text>().text = playerStats.BaseStrenght.ToString();
        agiBase.GetComponent<Text>().text = playerStats.BaseAgility.ToString();
        intBase.GetComponent<Text>().text = playerStats.BaseInteligence.ToString();
        vitBase.GetComponent<Text>().text = playerStats.BaseVitality.ToString();
    }
    private void DisplayItemAtributes()
    {
        
        strItem.GetComponent<Text>().text = "+" + playerEquip.strBonus;
        agiItem.GetComponent<Text>().text = "+" + playerEquip.agiBonus;
        intItem.GetComponent<Text>().text = "+" + playerEquip.intBonus;
        vitItem.GetComponent<Text>().text = "+" + playerEquip.vitBonus;
    }

    private void DisplayBaseStatistics()
    {
        
        hpBase.GetComponent<Text>().text = (playerStats.CalculateMaxHpPerLevel() + playerStats.CalculateMaxHpPerVit(playerStats.Vitality)).ToString();
        manaBase.GetComponent<Text>().text = (playerStats.CalculateMaxHpPerLevel() + playerStats.CalculateMaxManaPerInt(playerStats.Inteligence)).ToString();
        hpregBase.GetComponent<Text>().text = (playerStats.CalculateHealthRegenPerLevel() + playerStats.CalculateHealthRegenPerVit(playerStats.Vitality)).ToString();
        manaregBase.GetComponent<Text>().text = (playerStats.CalculateManaRegenPerLevel() + playerStats.CalculateManaRegenPerInt(playerStats.Inteligence)).ToString();
        closedmgBase.GetComponent<Text>().text = (playerStats.CalculateDamageClosePerLevel() + playerStats.CalculateDamageClosePerStr(playerStats.Strenght)).ToString();
        rangeddmgBase.GetComponent<Text>().text = (playerStats.CalculateDamageRangePerLevel() + playerStats.CalculateDamageRangedPerAgi(playerStats.Agility)).ToString();
        magicdmgBase.GetComponent<Text>().text = (playerStats.CalculateDamageMagicPerLevel() + playerStats.CalculateDamageMagicPerInt(playerStats.Inteligence)).ToString();
        aspdBase.GetComponent<Text>().text = (playerStats.CalculateAttackSpeedPerLevel() + playerStats.CalculateAttackSpeedPerAgi(playerStats.Agility)).ToString();
        defenceBase.GetComponent<Text>().text = (playerStats.CalculateArmorPerLevel() + playerStats.CalculatePhysicArmorPerVit(playerStats.Vitality)).ToString();
        magicdefencebase.GetComponent<Text>().text = (playerStats.CalculateMagicArmorPerInt(playerStats.Inteligence)).ToString();

    }

    public void SetAllUpZero()
    {
        
        closedmgUp.GetComponent<Text>().text = "+0";
        defenceUp.GetComponent<Text>().text = "+0";
        rangeddmgUp.GetComponent<Text>().text = "+0";
        aspdUp.GetComponent<Text>().text = "+0";
        manaUp.GetComponent<Text>().text = "+0";
        manaregUp.GetComponent<Text>().text = "+0";
        magicdmgUp.GetComponent<Text>().text = "+0";
        magicdefenceUp.GetComponent<Text>().text = "+0";
        hpUp.GetComponent<Text>().text = "+0";
        hpregUp.GetComponent<Text>().text = "+0";
        closedmgUp.GetComponent<Text>().color = Color.white;
        defenceUp.GetComponent<Text>().color = Color.white;
        rangeddmgUp.GetComponent<Text>().color = Color.white;
        aspdUp.GetComponent<Text>().color = Color.white;
        manaUp.GetComponent<Text>().color = Color.white;
        manaregUp.GetComponent<Text>().color = Color.white;
        magicdmgUp.GetComponent<Text>().color = Color.white;
        magicdefenceUp.GetComponent<Text>().color = Color.white;
        hpUp.GetComponent<Text>().color = Color.white;
        hpregUp.GetComponent<Text>().color = Color.white;

    }

    public void DisplayStrUp()
    {
        
        SetAllUpZero();
        closedmgUp.GetComponent<Text>().text = "+" + (playerStats.CalculateDamageClosePerStr(playerStats.Strenght + 1) - playerStats.CalculateDamageClosePerStr(playerStats.Strenght)).ToString();
        defenceUp.GetComponent<Text>().text = "+" + (playerStats.CalculatePhysicArmorPerVit(playerStats.Vitality + 1) - playerStats.CalculatePhysicArmorPerVit(playerStats.Vitality)).ToString();
        closedmgUp.GetComponent<Text>().color = Color.green;
        defenceUp.GetComponent<Text>().color = Color.green;
    }

    public void DisplayAgiUp()
    {
        
        SetAllUpZero();
        rangeddmgUp.GetComponent<Text>().text = "+" + (playerStats.CalculateDamageRangedPerAgi(playerStats.Agility + 1) - playerStats.CalculateDamageRangedPerAgi(playerStats.Agility)).ToString();
        aspdUp.GetComponent<Text>().text = "+" + (playerStats.CalculateAttackSpeedPerAgi(playerStats.Agility + 1) - playerStats.CalculateAttackSpeedPerAgi(playerStats.Agility)).ToString();
        rangeddmgUp.GetComponent<Text>().color = Color.green;
        aspdUp.GetComponent<Text>().color = Color.green;
    }

    public void DisplayIntUp()
    {
        
        SetAllUpZero();
        manaUp.GetComponent<Text>().text = "+" + (playerStats.CalculateMaxManaPerInt(playerStats.Inteligence + 1) - playerStats.CalculateMaxManaPerInt(playerStats.Inteligence)).ToString();
        manaregUp.GetComponent<Text>().text = "+" + (playerStats.CalculateManaRegenPerInt(playerStats.Inteligence + 1) - playerStats.CalculateManaRegenPerInt(playerStats.Inteligence)).ToString();
        magicdmgUp.GetComponent<Text>().text = "+" + (playerStats.CalculateDamageMagicPerInt(playerStats.Inteligence + 1) - playerStats.CalculateMagicArmorPerInt(playerStats.Inteligence)).ToString();
        magicdefenceUp.GetComponent<Text>().text = "+" + (playerStats.CalculateMagicArmorPerInt(playerStats.Inteligence + 1) - playerStats.CalculateMagicArmorPerInt(playerStats.Inteligence)).ToString();
        manaUp.GetComponent<Text>().color = Color.green;
        manaregUp.GetComponent<Text>().color = Color.green;
        magicdmgUp.GetComponent<Text>().color = Color.green;
        magicdefenceUp.GetComponent<Text>().color = Color.green;
    }

    public void DisplayVitUp()
    {
        
        SetAllUpZero();
        hpUp.GetComponent<Text>().text = "+" + (playerStats.CalculateMaxHpPerVit(playerStats.Vitality + 1) - playerStats.CalculateMaxHpPerVit(playerStats.Vitality)).ToString();
        hpregUp.GetComponent<Text>().text = "+" + (playerStats.CalculateHealthRegenPerVit(playerStats.Vitality + 1) - playerStats.CalculateHealthRegenPerVit(playerStats.Vitality)).ToString();
        hpUp.GetComponent<Text>().color = Color.green;
        hpregUp.GetComponent<Text>().color = Color.green;
    }

    private void DisplayItemStatistics()
    {
        
        hpItem.GetComponent<Text>().text = "+" + (playerEquip.maxHp + (playerEquip.maxHpRate-1) * (playerStats.CalculateMaxHpPerLevel() + playerStats.CalculateMaxHpPerVit(playerStats.Vitality)));
        manaItem.GetComponent<Text>().text = "+" + (playerEquip.maxMana + (playerEquip.maxManaRate - 1) * (playerStats.CalculateMaxManaPerLevel() + playerStats.CalculateMaxManaPerInt(playerStats.Inteligence)));
        manaregItem.GetComponent<Text>().text = "+" + (playerEquip.manaRegen + (playerEquip.manaRegenRate - 1) * (playerStats.CalculateMaxManaPerLevel() + playerStats.CalculateMaxManaPerInt(playerStats.Inteligence)));
        hpregItem.GetComponent<Text>().text = "+" + (playerEquip.hpRegen + (playerEquip.hpRegenRate - 1) * (playerStats.CalculateMaxHpPerLevel() + playerStats.CalculateMaxHpPerVit(playerStats.Vitality)));
        closeDmgItem.GetComponent<Text>().text = "+" + (playerEquip.dmgClose + (playerEquip.dmgCloseRate - 1) * (playerStats.CalculateDamageClosePerLevel() + playerStats.CalculateDamageClosePerStr(playerStats.Strenght)));
        rangeddmgItem.GetComponent<Text>().text = "+" + (playerEquip.dmgRanged + (playerEquip.dmgRangedRate - 1) * (playerStats.CalculateDamageRangePerLevel() + playerStats.CalculateDamageRangedPerAgi(playerStats.Agility)));
        magicdmgItem.GetComponent<Text>().text = "+" + (playerEquip.dmgMagic + (playerEquip.dmgMagicRate - 1) * (playerStats.CalculateDamageMagicPerLevel() + playerStats.CalculateDamageMagicPerInt(playerStats.Inteligence)));
        aspdItem.GetComponent<Text>().text = "+" + (playerEquip.aspd + (playerEquip.aspdRate - 1) * (playerStats.CalculateAttackSpeedPerLevel() + playerStats.CalculateAttackSpeedPerAgi(playerStats.Agility)));
        defenceItem.GetComponent<Text>().text = "+" + (playerEquip.physicalArmor + (playerEquip.physicalArmorRate - 1) * (playerStats.CalculateArmorPerLevel() + playerStats.CalculatePhysicArmorPerVit(playerStats.Vitality)));
        magicdefenceItem.GetComponent<Text>().text = "+" + (playerEquip.magicArmor + (playerEquip.magicArmorRate - 1) * playerStats.CalculateMagicArmorPerInt(playerStats.Inteligence));
    }

    
}
