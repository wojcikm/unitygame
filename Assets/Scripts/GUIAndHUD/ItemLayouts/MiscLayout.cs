﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MiscLayout : MonoBehaviour {

    public GameObject weightValue, priceValue;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }


    public void SetValues(Item item)
    {
        weightValue.GetComponent<Text>().text = item.Weight.ToString();
        priceValue.GetComponent<Text>().text = item.Price.ToString();
    }
}
