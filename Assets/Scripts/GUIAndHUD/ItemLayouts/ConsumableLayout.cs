﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConsumableLayout : MonoBehaviour {

    public GameObject typeValue, hpregenValue, manaregenValue, weightValue, priceValue, durationValue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetValues(Item item)
    {
        typeValue.GetComponent<Text>().text = item.Type.ToString();
        hpregenValue.GetComponent<Text>().text = item.ItemConsumable.HealthRegen.ToString();
        manaregenValue.GetComponent<Text>().text = item.ItemConsumable.ManaRegen.ToString();
        weightValue.GetComponent<Text>().text = item.Weight.ToString();
        priceValue.GetComponent<Text>().text = item.Price.ToString();
        durationValue.GetComponent<Text>().text = item.ItemConsumable.Duration.ToString();
        
    }
}
