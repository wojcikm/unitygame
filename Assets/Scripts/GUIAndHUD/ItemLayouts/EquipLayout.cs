﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EquipLayout : MonoBehaviour {

    public GameObject armorweaponValue, destinationValue, weightValue, priceValue, durabilityValue, defenceValue, minlvlValue, effectsValue;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetValues(Item item)
    {
        armorweaponValue.GetComponent<Text>().text = item.Type.ToString();
        destinationValue.GetComponent<Text>().text = item.ItemArmor.Class.ToString();
        weightValue.GetComponent<Text>().text = item.Weight.ToString();
        priceValue.GetComponent<Text>().text = item.Price.ToString();
        durabilityValue.GetComponent<Text>().text = item.ItemArmor.Durability.ToString();
        defenceValue.GetComponent<Text>().text = item.ItemArmor.Defence.ToString();
        minlvlValue.GetComponent<Text>().text = item.ItemArmor.RequiredLevel.ToString();
        effectsValue.GetComponent<Text>().text = item.ItemArmor.Effects.ToString();
    }
}
