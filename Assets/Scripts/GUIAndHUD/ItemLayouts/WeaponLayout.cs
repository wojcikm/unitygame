﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponLayout : MonoBehaviour {

    public GameObject armorweaponValue, destinationValue, weightValue, priceValue, damageValue, attackspeedValue, minlvlValue, effectsValue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetValues(Item item)
    {
        armorweaponValue.GetComponent<Text>().text = item.Type.ToString();
        destinationValue.GetComponent<Text>().text = item.ItemWeapon.Class.ToString();
        weightValue.GetComponent<Text>().text = item.Weight.ToString();
        priceValue.GetComponent<Text>().text = item.Price.ToString();
        damageValue.GetComponent<Text>().text = item.ItemWeapon.Damage.ToString();
        attackspeedValue.GetComponent<Text>().text = item.ItemWeapon.AttackSpeed.ToString();
        minlvlValue.GetComponent<Text>().text = item.ItemWeapon.RequiredLevel.ToString();
        effectsValue.GetComponent<Text>().text = item.ItemWeapon.Effects.ToString();
    }
}
