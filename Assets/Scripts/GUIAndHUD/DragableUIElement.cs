﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class DragableUIElement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private GUIHierarchyController hierarchy;

    public void OnBeginDrag(PointerEventData _EventData)
    {
        hierarchy = GameObject.Find("GUIElements").GetComponent<GUIHierarchyController>();
        hierarchy.SetOnTopOfHierarchy(gameObject.transform.parent.gameObject);
    }

    public void OnDrag(PointerEventData _EventData)
    {
        gameObject.GetComponent<RectTransform>().transform.position = new Vector3(_EventData.position.x, _EventData.position.y, 0);
    }

    public void OnEndDrag(PointerEventData _EventData)
    {
        
    }

}
