﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class InventoryListRightClick : MonoBehaviour, IPointerClickHandler{

    public UnityEvent leftClick;
    public UnityEvent rightClick;
    
    

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            leftClick.Invoke();
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
            rightClick.Invoke();
        
    }

    
}
