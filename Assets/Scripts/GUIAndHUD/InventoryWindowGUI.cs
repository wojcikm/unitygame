﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InventoryWindowGUI : MonoBehaviour {

    public GameObject ButtonEqup, ButtonConsumable, ButtonQuest, ButtonMisc;
    public List<GameObject> categoryButtons = new List<GameObject>();

    public GameObject PlayerEquipWindow;

    public GameObject ItemButtonPrefab;
    public GameObject InventoryGrid;
    public GameObject ItemListGameObject;
    public GameObject ItemDetailsGameObject;

    private InventoryControler inventory;
    private InventoryDetailView inventoryDetail;
    private PlayerEquip playerEquip;

    public GameObject ConsoleGameObject;
    public GameObject WindowGameObject;

    public List<Item> ActualList;
    
    public Sprite ButtonActive, ButtonUnactive;

    public enum InventoryType
    {
        none,
        equip,
        consum,
        quest,
        misc
    }
    public InventoryType ActiveItemType;

    private InventoryType SetActiveItemType(GameObject categoryButton)
    {
        InventoryType tempType = InventoryType.none;
        if (categoryButton == ButtonEqup)
            tempType = InventoryType.equip;
        else if (categoryButton == ButtonConsumable)
            tempType = InventoryType.consum;
        else if (categoryButton == ButtonQuest)
            tempType = InventoryType.quest;
        else if (categoryButton == ButtonMisc)
            tempType = InventoryType.misc;

        return tempType;
    }

    // Use this for initialization
    public void OpenInventoryWindow () {
        if (WindowGameObject.active == false)
        {
            WindowGameObject.SetActive(true);
            playerEquip = GameObject.Find("Player").GetComponent<PlayerEquip>();
            inventory = GameObject.Find("Player").GetComponent<InventoryControler>();
            inventoryDetail = GetComponent<InventoryDetailView>();
            categoryButtons = new List<GameObject>();
            categoryButtons.Add(ButtonEqup);
            categoryButtons.Add(ButtonConsumable);
            categoryButtons.Add(ButtonQuest);
            categoryButtons.Add(ButtonMisc);
            SetActiveItemType(ButtonEqup);
            SetActualSelected(ButtonEqup);
            ButtonEquipClicked();
            gameObject.transform.parent.GetComponent<GUIHierarchyController>().SetOnTopOfHierarchy(gameObject);
        }
        else
        {
            WindowGameObject.SetActive(false);
        }


    }

    

    // Update is called once per frame
    public void SetListActive(bool isListActive)
    {

    }

    public void SetActualSelected(GameObject button)
    {
        foreach(GameObject categoryButton in categoryButtons)
        {
            categoryButton.GetComponent<Image>().sprite = ButtonUnactive;
        }
        button.GetComponent<Image>().sprite = ButtonActive;
    }

    public void ButtonEquipClicked()
    {
        ActualList.Clear();
        ActualList = inventory.FilterInventory(Item.ItemType.Armor, Item.ItemType.Weapon);
        ShowItemList();
        SetActiveItemType(ButtonEqup);
        SetActualSelected(ButtonEqup);
    }

    public void ButtonConsumableClicked()
    {
        ActualList.Clear();
        
        ActualList = inventory.FilterInventory(Item.ItemType.Consumable);
        ShowItemList();
        SetActiveItemType(ButtonConsumable);
        SetActualSelected(ButtonConsumable);
    }

    public void ButtonQuestClicked()
    {
        ActualList.Clear();
        ActualList = inventory.FilterInventory(Item.ItemType.Quest);
        ShowItemList();
        SetActiveItemType(ButtonQuest);
        SetActualSelected(ButtonQuest);
    }

    public void ButtonMiscClicked()
    {
        ActualList.Clear();
        ActualList = inventory.FilterInventory(Item.ItemType.Misc);
        ShowItemList();
        SetActiveItemType(ButtonMisc);
        SetActualSelected(ButtonMisc);
    }

    

    public void ShowItemList()
    {
        ItemListGameObject.SetActive(true);
        ItemDetailsGameObject.SetActive(false);
        
        ClearItemList();
        foreach (Item item in ActualList)
        {
            GameObject button = Instantiate(ItemButtonPrefab, new Vector3(), new Quaternion()) as GameObject;
            button.transform.SetParent(InventoryGrid.transform, false);
            button.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = item.Name;
            button.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Text>().text = inventory.CalculateAmout(item).ToString();
            button.GetComponent<ItemScript>().item = item;
            button.GetComponent<Button>().onClick.AddListener(() => { ShowItemDetails(button.GetComponent<ItemScript>().item); });
            button.GetComponent<InventoryListRightClick>().rightClick.AddListener(() => { UseItem(button.GetComponent<ItemScript>().item); });
            

        }
    }

    public void UseItem(Item item)
    {
        if (item.Type == Item.ItemType.Armor || item.Type == Item.ItemType.Weapon)
        {
            playerEquip.EquipItem(item.ItemID);
            ShowItemList();
            ActualList.Clear();
            ActualList = inventory.FilterInventory(Item.ItemType.Armor, Item.ItemType.Weapon);
            ShowItemList();
            SendConsoleMessage(item.Name + " equipped", Color.green, 5);
            if (PlayerEquipWindow.GetComponent<EquipmentWindow>().WindowGameObject.active == true)
            {
                PlayerEquipWindow.GetComponent<EquipmentWindow>().SetEquipment();
            }
        }
        else
        {
            SendConsoleMessage("Can't use this item", Color.red, 5);
        }
    }

    public void ClearItemList()
    {
        for(int i=0;i<InventoryGrid.transform.childCount;i++)
        {
           Destroy(InventoryGrid.transform.GetChild(i).gameObject);
        }
    }

    public void ShowItemDetails(Item item)
    {
        ItemListGameObject.SetActive(false);
        ItemDetailsGameObject.SetActive(true);
        inventoryDetail.SetItemValues(item);
        
    }

    public void SendConsoleMessage(string message, Color color, int time)
    {
        ConsoleGameObject.GetComponent<Text>().text = message;
        ConsoleGameObject.GetComponent<Text>().color = color;
        StartCoroutine(ClearConsole(time));

    }

    

    IEnumerator ClearConsole(int time)
    {
        yield return new WaitForSeconds(time);
        ConsoleGameObject.GetComponent<Text>().text = "";
    }

    private void ClearConsole()
    {
        ConsoleGameObject.GetComponent<Text>().text = "";
    }


}
