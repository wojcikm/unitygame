﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthManaController : MonoBehaviour {

    public Image Health;
    public Image Mana;
    public Text CurrentLevel;
    public Text CurrentExp;
    private Text MaxExp;
    public PlayerStats playerStats;
    private Text CurrentStr;
    private Text CurrentAgi;
    private Text CurrentVit;
    private Text CurrentInt;
    private Text CurrentStatPoints;

    private Text CurrentHpRegen;
    private Text CurrentManaRegen;
    private Text CurrentDamageClose;
    private Text CurrentDamageRanged;
    private Text CurrentDamageMagic;

    private Text CurrentAttackSpeed;
    private Text CurrentPhysicalArmor;
    private Text CurrentMagicArmor;

	// Use this for initialization
	void Start () {
        Health = GameObject.Find("Health").GetComponent<Image>();
        Mana = GameObject.Find("Mana").GetComponent<Image>();
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        CurrentLevel = GameObject.Find("CurrentLevel").GetComponent<Text>();
        MaxExp = GameObject.Find("MaxExp").GetComponent<Text>();
        CurrentExp = GameObject.Find("CurrentExp").GetComponent<Text>();
        CurrentStr = GameObject.Find("CurrentStr").GetComponent<Text>();
        CurrentAgi = GameObject.Find("CurrentAgi").GetComponent<Text>();
        CurrentVit = GameObject.Find("CurrentVit").GetComponent<Text>();
        CurrentInt = GameObject.Find("CurrentInt").GetComponent<Text>();
        CurrentStatPoints = GameObject.Find("StatPointsCurrent").GetComponent<Text>();
        CurrentHpRegen = GameObject.Find("CurrentHpPerSecond").GetComponent<Text>();
        CurrentManaRegen = GameObject.Find("CurrentManaPerSecond").GetComponent<Text>();
        CurrentDamageClose = GameObject.Find("CurrentDamageClose").GetComponent<Text>();
        CurrentDamageRanged = GameObject.Find("CurrentDamageRanged").GetComponent<Text>();
        CurrentDamageMagic = GameObject.Find("CurrentDamageMagic").GetComponent<Text>();

        CurrentAttackSpeed = GameObject.Find("CurrentAttackSpeed").GetComponent<Text>();
        CurrentPhysicalArmor = GameObject.Find("CurrentPhysicalArmor").GetComponent<Text>();
        CurrentMagicArmor = GameObject.Find("CurrentMagicArmor").GetComponent<Text>();

    }
	
	// Update is called once per frame
	void Update () {
        Health.fillAmount = playerStats.CurrentHealth / playerStats.MaxHealth;
        Mana.fillAmount = playerStats.CurrentMana / playerStats.MaxMana;
        CurrentLevel.text = playerStats.CurrentLevel.ToString();
        CurrentExp.text = playerStats.CurrentExperience.ToString();
        MaxExp.text = playerStats.MaxExperience.ToString();
        CurrentStr.text = playerStats.Strenght.ToString();
        CurrentAgi.text = playerStats.Agility.ToString();
        CurrentVit.text = playerStats.Vitality.ToString();
        CurrentInt.text = playerStats.Inteligence.ToString();
        CurrentStatPoints.text = playerStats.StatisticPoints.ToString();
        CurrentHpRegen.text = playerStats.HealthPerSecond.ToString();
        CurrentManaRegen.text = playerStats.ManaPerSecond.ToString();
        CurrentDamageClose.text = playerStats.DamageClose.ToString();
        CurrentDamageRanged.text = playerStats.DamageRanged.ToString();
        CurrentDamageMagic.text = playerStats.DamageMagic.ToString();

        CurrentAttackSpeed.text = playerStats.AttackSpeed.ToString();
        CurrentPhysicalArmor.text = playerStats.PhysicalArmor.ToString();
        CurrentMagicArmor.text = playerStats.MagicArmor.ToString();
    }
}
