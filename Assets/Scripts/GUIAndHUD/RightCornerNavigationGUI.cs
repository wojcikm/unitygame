﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RightCornerNavigationGUI : MonoBehaviour {

    public GameObject InventoryWindow;
    public GameObject QuestWindow;
    public GameObject PlayerStatsWindow;
    public GameObject EquipmentWindow;

    public GameObject InventoryButton;
    public GameObject QuestButton;
    public GameObject PlayerStatsButton;
    public GameObject EquipmentButton;

    public List<GameObject> ButtonsList ;
    public List<GameObject> WindowsList ;

	// Use this for initialization
	void Start () {       


        ButtonsList.Add(InventoryButton);
        ButtonsList.Add(QuestButton);
        ButtonsList.Add(PlayerStatsButton);
        ButtonsList.Add(EquipmentButton);

        WindowsList.Add(InventoryWindow);
        WindowsList.Add(QuestWindow);
        WindowsList.Add(PlayerStatsWindow);
        WindowsList.Add(EquipmentWindow);
    }
	
	// Update is called once per frame
	void OnGUI ()
    {
       UpdateButtonsActive();
       CheckMouseOverElement();
    }

    private void UpdateButtonsActive()
    {
        

        if (InventoryWindow && InventoryWindow.active == true)
            InventoryButton.GetComponent<Button>().image.color = Color.green;
        else
            InventoryButton.GetComponent<Button>().image.color = Color.white;

        if (QuestWindow && QuestWindow.active == true)
            QuestButton.GetComponent<Button>().image.color = Color.green;
        else
            QuestButton.GetComponent<Button>().image.color = Color.white;

        if (PlayerStatsWindow && PlayerStatsWindow.active == true)
            PlayerStatsButton.GetComponent<Button>().image.color = Color.green;
        else
            PlayerStatsButton.GetComponent<Button>().image.color = Color.white;

        if (EquipmentWindow && EquipmentWindow.active == true)
            EquipmentButton.GetComponent<Button>().image.color = Color.green;
        else
            EquipmentButton.GetComponent<Button>().image.color = Color.white;
    }

    private void CheckMouseOverElement()
    {
        
        for(int i=0;i<ButtonsList.Count;i++)
        {
            
            if (EventSystem.current.IsPointerOverGameObject(ButtonsList[i].GetInstanceID()) && !WindowsList[i].active)
            {
                ButtonsList[i].GetComponent<Button>().image.color = Color.blue;
            }
        }
    }
}
