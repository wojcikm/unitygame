﻿using UnityEngine;
using System.Collections;

public class GUIHierarchyController : MonoBehaviour {

    // Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetOnTopOfHierarchy(GameObject window)
    {
        int childCount = gameObject.transform.childCount;
        window.transform.SetAsLastSibling();
        for(int i=0;i<childCount;i++)
        {
            gameObject.transform.GetChild(i).GetComponent<Canvas>().sortingOrder = i;
        }        
    }
}
