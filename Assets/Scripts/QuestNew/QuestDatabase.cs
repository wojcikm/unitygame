﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class QuestDatabase : MonoBehaviour
{

    public List<Quest> database;
    public ObjectiveDatabase objectiveList;
    private JsonData questData;

    void Start()
    {
        objectiveList = GameObject.Find("ObjectiveDatabase").GetComponent<ObjectiveDatabase>();
        AddObjectives();
    }
    void Awake()
    {
        questData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Quests.json"));
        ConstructQuestDatabase();
        
        
    }
    void AddObjectives()
    {
        database[0].Objectives.Add(objectiveList.database[0]);
        database[0].Objectives.Add(objectiveList.database[1]);
        database[0].Objectives.Add(objectiveList.database[2]);
        database[0].Objectives.Add(objectiveList.database[3]);
        database[0].Objectives.Add(objectiveList.database[4]);
        database[1].Objectives.Add(objectiveList.database[0]);
        database[1].Objectives.Add(objectiveList.database[1]);
        database[1].Objectives.Add(objectiveList.database[2]);
    }
    void ConstructQuestDatabase()
    {
        for (int i = 0; i < questData.Count; i++)
        {
            database.Add(new Quest(
                questData[i]["name"].ToString(),
                questData[i]["descriptionSummary"].ToString(),
                questData[i]["dialog"].ToString(),
                (int)questData[i]["questID"],
                (int)questData[i]["chainQuestID"],
                (int)questData[i]["sourceID"],
                (int)questData[i]["endID"],
                (int)questData[i]["rewardExp"],
                (int)questData[i]["rewardItem"],
                (int)questData[i]["rewardItemAmount"], 
                (int)questData[i]["rewardGold"],                            
                (bool)questData[i]["main"]));
            
        }
    }

    public Quest FetchQuestById(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (id == database[i].QuestID)
                return database[i];
        }

        return null;
    }

    public Quest FetchQuestByName(string name)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (name == database[i].Name)
                return database[i];
        }

        return null;
    }
}