﻿using UnityEngine;
using System.Collections;
using System;

public class ObjectiveController : MonoBehaviour {

    private ObjectiveDatabase objectiveDatabase;
    private QuestLog questList;
    private InventoryControler inventory;
    private PlayerStats playerStats;
    

    void Start()
    {
        
        objectiveDatabase = GameObject.Find("ObjectiveDatabase").GetComponent<ObjectiveDatabase>();
        questList = GameObject.Find("Player").GetComponent<QuestLog>();
        inventory = GameObject.Find("Player").GetComponent<InventoryControler>();
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
    }

	void Update()
    {
        
    }

    public void UpdateGoProgress(int objectiveID)
    {
        for (int i = 0; i < questList.QuestsLog.Count; i++)
        {
            for (int j = 0; j < questList.QuestsLog[i].Objectives.Count; j++)
            {
                if (objectiveID == questList.QuestsLog[i].Objectives[j].ObjectiveID && questList.QuestsLog[i].Objectives[j].Type == Objective.ObjectiveType.Go)
                {
                    questList.QuestsLog[i].Objectives[j].IsComplete = true;
                }
            }
        }
    }

    public void UpdateCollectProgress()
    {
        for (int i = 0; i < questList.QuestsLog.Count; i++)
        {
            for (int j = 0; j < questList.QuestsLog[i].Objectives.Count;j++)
            {
                if (questList.QuestsLog[i].Objectives[j].Type == Objective.ObjectiveType.Collect)
                {
                    for (int k = 0; k < inventory.inventory.Count; k++)
                    {
                        if (inventory.inventory[k].ItemID == questList.QuestsLog[i].Objectives[j].ObjectiveCollect.ItemToCollect)
                        {
                            try
                            {
                                questList.QuestsLog[i].Objectives[j].ObjectiveCollect.CurrentAmountCollected = inventory.inventory[k].ItemQuest.Amount;
                                if (questList.QuestsLog[i].Objectives[j].ObjectiveCollect.CurrentAmountCollected >= questList.QuestsLog[i].Objectives[j].ObjectiveCollect.AmountToCollect)
                                {
                                    questList.QuestsLog[i].Objectives[j].IsComplete = true;

                                }
                                else
                                {
                                    questList.QuestsLog[i].Objectives[j].IsComplete = false;

                                }
                            }
                            catch (Exception e)
                            {
                                Debug.Log(e + "Exception caught.");
                            }
                        }

                    }
                }
            }
        }


    }
    public void UpdateMonsterKillProgress(int monsterID)
    {
        for (int i = 0; i < questList.QuestsLog.Count; i++)
        {
            for (int j = 0; j < questList.QuestsLog[i].Objectives.Count; j++)
            {
                if (questList.QuestsLog[i].Objectives[j].Type == Objective.ObjectiveType.Kill)
                {
                    if (questList.QuestsLog[i].Objectives[j].ObjectiveKillMonster.MonsterToKill == monsterID)
                    {
                        if (questList.QuestsLog[i].Objectives[j].ObjectiveKillMonster.CurrentMonsterKill < questList.QuestsLog[i].Objectives[j].ObjectiveKillMonster.AmountToKill)
                            questList.QuestsLog[i].Objectives[j].ObjectiveKillMonster.CurrentMonsterKill++;
                        if (questList.QuestsLog[i].Objectives[j].ObjectiveKillMonster.CurrentMonsterKill >= questList.QuestsLog[i].Objectives[j].ObjectiveKillMonster.AmountToKill)
                        {
                            questList.QuestsLog[i].Objectives[j].IsComplete = true;
                        }
                    }
                }
            }
        }
    }
    public void EndObjective(int EndNpcID)
    {
        for (int i = 0; i < questList.QuestsLog.Count; i++)
        {
            for (int j = 0; j < questList.QuestsLog[i].Objectives.Count; j++)
            {
                if(questList.QuestsLog[i].Objectives[j].IsComplete == true)
                    {
                        questList.UpdateQuestsProgress();
                    }
                }
            }
        }
    }
       
   

    

