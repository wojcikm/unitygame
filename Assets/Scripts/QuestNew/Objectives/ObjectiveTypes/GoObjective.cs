﻿using UnityEngine;
using System.Collections;

public class GoObjective {

    public enum Area
    {
        None,
        Square,
        Circle
    }
    public Vector3 PlaceToGo; //vector3 miejsce do dotarcia
    public int TimeToGo; // czas na dotarcie
    public Area ActivateArea;
    


    public GoObjective(Vector3 placeToGo, int timeToGo, Area activateArea)
    {

        this.ActivateArea = activateArea;
        this.PlaceToGo = placeToGo;
        this.TimeToGo = timeToGo;
        

    }


}
