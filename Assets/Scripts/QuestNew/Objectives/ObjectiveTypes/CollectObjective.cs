﻿using UnityEngine;
using System.Collections;
using System;

public class CollectObjective {


    public int ItemToCollect; //id of item to collect
    public int CurrentAmountCollected; //current amount of collected items in inventory
    public int AmountToCollect; //amount of items to collect 
    public int MonsterDropHint; //id of a monster that drops needed item


   
    public CollectObjective(int itemToCollect, int amountToCollect, int monsterDropHint)
    {        
        this.ItemToCollect = itemToCollect;
        this.CurrentAmountCollected = 0;
        this.AmountToCollect = amountToCollect;
        this.MonsterDropHint = monsterDropHint;
            }


    public int GetItemToCollectId()
    {
        return ItemToCollect;

    }
    public void IncreaseItemCollected()
    {
        CurrentAmountCollected++;

    }










}
