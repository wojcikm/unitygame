﻿using UnityEngine;
using System.Collections;

public class MonsterKillObjective {

    public int MonsterToKill; //id of monster to kill
    public int CurrentMonsterKill; //current amount of monsters killed
    public int AmountToKill; //amount of monsters to kill


    public MonsterKillObjective(int monsterToKill,
        int amountToKill)
    {
        this.MonsterToKill = monsterToKill;
        this.CurrentMonsterKill = 0;
        this.AmountToKill = amountToKill;
    }
}
