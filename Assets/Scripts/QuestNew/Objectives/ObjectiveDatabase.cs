﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using LitJson;


public class ObjectiveDatabase : MonoBehaviour
{

    public List<Objective> database = new List<Objective>();
    private JsonData objectiveData;

    void Awake()
    {
        objectiveData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Objectives.json"));
        ConstructObjectiveDatabase();

    }

    void ConstructObjectiveDatabase()
    {
        for (int i = 0; i < objectiveData.Count; i++)
        {
            //collect
            if ((Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()) == Objective.ObjectiveType.Collect)
            {
                database.Add(new Objective(
                     new CollectObjective(
                         (int)objectiveData[i]["itemToCollect"],
                         (int)objectiveData[i]["amountToCollect"],
                         (int)objectiveData[i]["monsterDropHint"]),
                     (Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()),
                     (int)objectiveData[i]["objectiveID"],
                     (bool)objectiveData[i]["isMain"]));

            }
            //kill
            else if ((Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()) == Objective.ObjectiveType.Kill)
            {

                database.Add(new Objective(
                     new MonsterKillObjective(
                         (int)objectiveData[i]["monsterToKill"],
                         (int)objectiveData[i]["amountToKill"]),                         
                     (Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()),
                     (int)objectiveData[i]["objectiveID"],
                     (bool)objectiveData[i]["isMain"]));
            }
            else if ((Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()) == Objective.ObjectiveType.Talk)
            {

                database.Add(new Objective(
                     new NPCTalkObjective(
                         (int)objectiveData[i]["nodeIDToOpen"]), 
                     (Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()),
                     (int)objectiveData[i]["objectiveID"],
                     (bool)objectiveData[i]["isMain"]));
            }
            else if ((Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()) == Objective.ObjectiveType.Go)
            {

                /*database.Add(new Objective(
                     new GoObjective(
                         (int)objectiveData[i]["itemToCollect"],

                         (int)objectiveData[i]["amountToCollect"],
                         (int)objectiveData[i]["monsterDropHint"]),
                     (Objective.ObjectiveType)Enum.Parse(typeof(Objective.ObjectiveType), objectiveData[i]["type"].ToString()),
                     objectiveData[i]["name"].ToString(),
                     objectiveData[i]["descriptionSummary"].ToString(),
                     (int)objectiveData[i]["objectiveID"],
                     (int)objectiveData[i]["chainObjectiveID"],
                     (int)objectiveData[i]["sourceID"],
                     (int)objectiveData[i]["endID"],
                     (int)objectiveData[i]["rewardExp"],
                     (int)objectiveData[i]["rewardItem"],
                     (int)objectiveData[i]["rewardItemAmount"],
                     (bool)objectiveData[i]["isMain"]));*/

            }
        }
    }
            

    public Objective FetchObjectiveById(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (id == database[i].ObjectiveID)
                return database[i];
        }

        return null;
    }

    
}
