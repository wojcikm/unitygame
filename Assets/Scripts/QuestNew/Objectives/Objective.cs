﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Objective
{

    public enum ObjectiveType
    {
        None,
        Collect,
        Kill,
        Go,
        Talk
    }

    public ObjectiveType Type;        //objective type(collect, kill go, talk)
    //public string Name; //objective name
    //public string DescriptionSummary; //objective description
    
    public int ObjectiveID; //objective id
    //public int ChainObjectiveID; //id of objective afterwards (0 if no chain objectives)
   // public int SourceID;// id of npc that we receive the quest from
    //public int EndID; //id of npc that we return objective to

    //public int RewardExp; //experience reward
    //public int RewardItem; // id of reward item
    //public int RewardItemAmount; //amount of items received as reward

    public bool IsMain; //main objective or not
    public bool IsComplete; //if objective is complete or not
    //public bool IsReturned; //if objective is returned to npc or not

    public CollectObjective ObjectiveCollect;
    public GoObjective ObjectiveGo;
    public MonsterKillObjective ObjectiveKillMonster;
    public NPCTalkObjective ObjectiveTalk;

    public Objective(CollectObjective objectiveCollect, ObjectiveType type, int objectiveID, bool isMain)
    {
        this.ObjectiveCollect = objectiveCollect;
        this.Type = type;
        this.ObjectiveID = objectiveID;
       
        this.IsMain = isMain;
        this.IsComplete = false;
        
    }

    public Objective(MonsterKillObjective objectiveKill, ObjectiveType type, int objectiveID, bool isMain)
    {
        this.ObjectiveKillMonster = objectiveKill;
        this.Type = type;       
        this.ObjectiveID = objectiveID;       
        this.IsMain = isMain;
        this.IsComplete = false;
      
    }
    public Objective(NPCTalkObjective objectiveTalk, ObjectiveType type, int objectiveID, bool isMain)
    {
        this.ObjectiveTalk = objectiveTalk;
        this.Type = type;
        this.ObjectiveID = objectiveID;
        this.IsMain = isMain;
        this.IsComplete = false;
     
    }
    public Objective(GoObjective objectiveGo, ObjectiveType type, int objectiveID, bool isMain)
    {
        this.ObjectiveGo = objectiveGo;
        this.Type = type;
        this.ObjectiveID = objectiveID;
        this.IsMain = isMain;
        this.IsComplete = false;
     
    }

       
    public void UpdateItemCollection()
    {
        
    }
   
}
 