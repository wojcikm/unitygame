﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestLog : MonoBehaviour {


    public List<Quest> QuestsLog;
    public InventoryControler inventory;
    private PlayerStats playerStats;
    
    private QuestDatabase baza;
    
    // Use this for initialization
    void Start()
    {
        baza = GameObject.Find("QuestDatabase").GetComponent<QuestDatabase>();
        inventory = GameObject.Find("Player").GetComponent<InventoryControler>();
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        
        AddQuest(1);
        AddQuest(2);
        AddQuest(3);
        AddQuest(4);
        AddQuest(5);
        AddQuest(6);
        AddQuest(7);
        AddQuest(8);
        AddQuest(9);
        AddQuest(10);
        AddQuest(11);
        AddQuest(12);

    }
	
	public void AddQuest(int i)
    {
        
                QuestsLog.Add(baza.FetchQuestById(i));
            
            //QuestAlreadyOnList
        
        
    }
    public void RemoveQuest(int i)
    {
        foreach (Quest quest in QuestsLog)
        {
            if (quest == baza.database.Find(x => x.QuestID == i))
            {
                QuestsLog.Remove(QuestsLog.Find(x => x.QuestID == i));
            }
            //quest not on list
        }
    }

    public Quest FetchQuestByName(string name)
    {
        for (int i = 0; i < baza.database.Count; i++)
        {
            if (name == baza.database[i].Name)
                return baza.database[i];
        }

        return null;
    }

    public void UpdateQuestsProgress()
    {
        int mainObjectives = 0;
        int mainObjectivesDone = 0;
        for(int i=0;i<QuestsLog.Count; i++)
        {
            for(int j=0;j<QuestsLog[i].Objectives.Count;j++)
            {
                if (QuestsLog[i].Objectives[j].IsMain)
                    mainObjectives++;
            }
            for (int j = 0; j < QuestsLog[i].Objectives.Count; j++)
            {
                if (QuestsLog[i].Objectives[j].IsMain)
                    mainObjectivesDone++;
            }
            if(mainObjectives == mainObjectivesDone)
            {
                QuestsLog[i].IsComplete = true;
            }
        }
    }

    public void EndQuests(int endNPC)
    {
        for (int i = 0; i < QuestsLog.Count; i++)
        {
            if(QuestsLog[i].IsComplete == true && endNPC == QuestsLog[i].EndID)
            {
                for(int j=0;j<QuestsLog[i].RewardItemAmount;j++)
                {
                    inventory.AddItem(QuestsLog[i].RewardItem);
                }
                playerStats.GainExperience(QuestsLog[i].RewardExp);
                QuestsLog[i].ReturnQuest();
                if (QuestsLog[i].ChainQuestID != 0)
                {
                    QuestsLog.Add(baza.FetchQuestById(QuestsLog[i].ChainQuestID));
                }
                //QuestsLog.Remove(QuestsLog[i]);
            }
        }
    }
}
