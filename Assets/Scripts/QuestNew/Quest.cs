﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Quest
{
    public string Name; //quest name
    public string DescriptionSummary; //quest long description
    public string Dialog; //quest short description
    
    public int QuestID; //id of a quest
    public int ChainQuestID; //id of quest after finishing current one (0 if no chain quest)
    public int SourceID; //id of npc we receive quest from
    public int EndID; // id of npc we return quest to

    public int RewardExp; //experience reward for finishing the quest
    public int RewardItem; //id of item we receive for finishing the quest
    public int RewardItemAmount; //amount of items we receive fror finishing the quest
    public int RewardGold;

    public bool IsMain; //if quest is main or not
    public bool IsComplete; //if quest is complete or not
    public bool IsReturned; //if quest was returned to npc or not

    public List<Objective> Objectives; //list of objectives among current quest

        public Quest(string name, string descriptionSummary, string dialog, int questID, int chainQuestID, 
            int sourceID, int endID, int rewardExp, int rewardItem, int rewardItemAmount, int rewardGold, bool isMain)
    {
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
        this.Dialog = dialog;
        this.QuestID = questID;
        this.ChainQuestID = chainQuestID;
        this.SourceID = sourceID;
        this.EndID = endID;
        this.RewardExp = rewardExp;
        this.RewardItem = rewardItem;
        this.RewardItemAmount = rewardItemAmount;
        this.RewardGold = rewardGold;
        this.IsMain = isMain;

        this.Objectives = new List<Objective>();
    }

    public Quest()
    {
        this.QuestID = -1;
    }

    public void ReturnQuest()
    {
        this.IsReturned = true;
    }

}
