﻿using UnityEngine;
using System.Collections;

public class FlashLightMovement : MonoBehaviour {

 public float MoveAmount = 1.0f;
 public float MoveSpeed  = 2.0f;
    public GameObject GunObject;
    public float MoveOnX;
    public float MoveOnY;
    public Vector3 DefaultPos;
    public Vector3 NewGunPos;

	// Use this for initialization
	void Start () {
        DefaultPos = transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
        MoveOnX = Input.GetAxis("Mouse X") * Time.deltaTime * MoveAmount;
        MoveOnY = Input.GetAxis("Mouse Y") * Time.deltaTime * MoveAmount;
        NewGunPos = new Vector3(DefaultPos.x + MoveOnX, DefaultPos.y + MoveOnY, DefaultPos.z);
        GunObject.transform.localPosition = Vector3.Lerp(GunObject.transform.localPosition, NewGunPos, MoveSpeed * Time.deltaTime);

        
    }
}
