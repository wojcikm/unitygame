﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ShopNPC {

    public List<int> ShopItems;
    public int ShopTax;
    public int ShopMoney;

    public ShopNPC(List<int> shopItems,int shopTax, int shopMoney)
    {
        this.ShopItems = shopItems;
        this.ShopTax = shopTax;
        this.ShopMoney = shopMoney;
    }

    public void ReceiveGold(int amount)
    {
        ShopMoney += amount;
    }

    public void SpendGold(int amount)
    {
        ShopMoney -= amount;
    }
}
