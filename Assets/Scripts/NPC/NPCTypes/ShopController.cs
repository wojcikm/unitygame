﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopController : MonoBehaviour {

    public int npcID;
    private InventoryControler inventory;
    private ItemDatabase itemDatabase;
    
    private ObjectiveDatabase objectiveDatabase;
    private ObjectiveController objectiveController;
    private QuestLog questLog;
    private NPC npc;

    // Use this for initialization
    void Start () {
        questLog = GameObject.Find("Player").GetComponent<QuestLog>();
        inventory = GameObject.Find("Player").GetComponent<InventoryControler>();
        itemDatabase = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();
        
        objectiveDatabase = GameObject.Find("ObjectiveDatabase").GetComponent<ObjectiveDatabase>();
        objectiveController = GameObject.Find("ObjectiveController").GetComponent<ObjectiveController>();
        npc = GameObject.Find("NPCDatabase").GetComponent<NPCDatabase>().FetchNPCById(npcID);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ReceiveNewQuest(int quest)
    {
        questLog.AddQuest(quest);            
    }

    public void ReceiveNewObjective(int quest, int objective)
    {
        for (int i = 0; i < questLog.QuestsLog.Count; i++)
        {
            questLog.QuestsLog[i].Objectives.Add(objectiveDatabase.FetchObjectiveById(objective));
        }
    }

    public void FinishQuest(int quest)
    {
        for(int i=0;i<questLog.QuestsLog.Count;i++)
        {
            if(quest == questLog.QuestsLog[i].QuestID && questLog.QuestsLog[i].EndID == npc.NPCID)
            {
                questLog.EndQuests(npc.NPCID);
            }
        }
    }

    public void FinishObjective(int objective)
    {
        for (int i = 0; i < questLog.QuestsLog.Count; i++)
        {
            for (int j = 0; j < questLog.QuestsLog[i].Objectives.Count; j++)
            {
                if (objective == questLog.QuestsLog[i].Objectives[j].ObjectiveID)
                {
                    objectiveController.EndObjective(npc.NPCID);
                }
            }
        }
    }

    public void BuyItem(int item)
    {
        for (int i = 0; i < npc.NPCShop.ShopItems.Count; i++)
        {
            if (npc.NPCShop.ShopItems[i] == item)
            {
                if (itemDatabase.FetchItemById(item).Price * (1 + npc.NPCShop.ShopTax / 100) <= inventory.Gold)
                {
                    inventory.AddItem(item);
                    inventory.SpendGold(itemDatabase.FetchItemById(item).Price * (1 + npc.NPCShop.ShopTax / 100));
                    npc.NPCShop.ReceiveGold(itemDatabase.FetchItemById(item).Price * (1 + npc.NPCShop.ShopTax / 100));
                }
                //Not Enough gold
            }
        }
    }
    public void SellItem(int item)
    {
        for (int i = 0; i < inventory.inventory.Count; i++)
        {
            if (inventory.inventory[i].ItemID == item)
            {
                if (inventory.inventory[i].Type != Item.ItemType.Quest)
                {
                    if (itemDatabase.FetchItemById(item).Price <= npc.NPCShop.ShopMoney)
                    {
                        inventory.RemoveItem(item);
                        inventory.ReceiveGold(itemDatabase.FetchItemById(item).Price);
                        npc.NPCShop.SpendGold(itemDatabase.FetchItemById(item).Price);
                    }

                    //not enough gold
                }
                //quest item
            }
        }
    }
}
