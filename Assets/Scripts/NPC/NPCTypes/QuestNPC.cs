﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestNPC{

    public List<int> QuestList;
    public List<int> ObjectiveList;

    public QuestNPC(List<int> questList, List<int> objectiveList)
    {
        this.QuestList = questList;
        this.ObjectiveList = objectiveList;
    }
	
}
