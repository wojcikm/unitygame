﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System;

public class NPCDatabase : MonoBehaviour {

    public List<NPC> database = new List<NPC>();
    private JsonData npcData;
	// Use this for initialization
	void Awake () {
        npcData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/NPC.json"));
        ConstructNpcDatabase();
    }

    private void ConstructNpcDatabase()
    {
        for(int i=0;i<npcData.Count;i++)
        {
            if((NPC.NPCType)Enum.Parse(typeof(NPC.NPCType), npcData[i]["type"].ToString()) == NPC.NPCType.Quest)
            {
                List<int> temporaryList1 = new List<int>();
                List<int> temporaryList2 = new List<int>();
                for (int j = 0; j < npcData[i]["questList"].Count; j++)
                {
                    temporaryList1.Add((int)npcData[i]["questList"][j]);
                }
                for (int j = 0; j < npcData[i]["objectiveList"].Count; j++)
                {
                    temporaryList1.Add((int)npcData[i]["objectiveList"][j]);
                }
                database.Add(new NPC(
                    new QuestNPC(
                        temporaryList1,
                        temporaryList2
                        ),
                    (NPC.NPCType)Enum.Parse(typeof(NPC.NPCType), npcData[i]["type"].ToString()),
                    (int)npcData[i]["npcID"],
                    npcData[i]["name"].ToString(),
                    npcData[i]["descriptionSummary"].ToString()));
            }
            else if((NPC.NPCType)Enum.Parse(typeof(NPC.NPCType), npcData[i]["type"].ToString()) == NPC.NPCType.Talk)
            {
                database.Add(new NPC(
                    new TalkNPC(
                        ),
                    (NPC.NPCType)Enum.Parse(typeof(NPC.NPCType), npcData[i]["type"].ToString()),
                    (int)npcData[i]["npcID"],
                    npcData[i]["name"].ToString(),
                    npcData[i]["descriptionSummary"].ToString()));
            }

            else if ((NPC.NPCType)Enum.Parse(typeof(NPC.NPCType), npcData[i]["type"].ToString()) == NPC.NPCType.Shop)
            {
                List<int> temporaryList = new List<int>();

                for (int j = 0; j < npcData[i]["shopItems"].Count; j++)
                {
                    temporaryList.Add((int)npcData[i]["shopItems"][j]);
                }
                database.Add(new NPC(
                    new ShopNPC(
                        temporaryList,
                        (int)npcData[i]["shopTax"],
                        (int)npcData[i]["shopMoney"]
                        ),
                    (NPC.NPCType)Enum.Parse(typeof(NPC.NPCType), npcData[i]["type"].ToString()),
                    (int)npcData[i]["npcID"],
                    npcData[i]["name"].ToString(),
                    npcData[i]["descriptionSummary"].ToString()));
            }



        }
    }
    public NPC FetchNPCById(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (id == database[i].NPCID)
                return database[i];
        }

        return null;
    }


}
