﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class NPC { 

    public enum NPCType
    {
        None,
        Quest,
        Talk,
        Shop
    }
    public NPCType Type;
    public int NPCID;
    public string Name;
    public string DescriptionSummary;

    

    public QuestNPC NPCQuest;
    public ShopNPC NPCShop;
    public TalkNPC NPCTalk;

    public NPC(QuestNPC npcQuest, NPCType type, int npcID, string name, string descriptionSummary)
    {
        this.NPCQuest = npcQuest;
        this.Type = type;
        this.NPCID = npcID;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
    }
    public NPC(ShopNPC npcShop, NPCType type, int npcID, string name, string descriptionSummary)
    {
        this.NPCShop = npcShop;
        this.Type = type;
        this.NPCID = npcID;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
    }
    public NPC(TalkNPC npcTalk, NPCType type, int npcID, string name, string descriptionSummary)
    {
        this.NPCTalk = npcTalk;
        this.Type = type;
        this.NPCID = npcID;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
    }




}
