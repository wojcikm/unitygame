﻿using UnityEngine;
using System.Collections;

public class ItemScript : MonoBehaviour {

    public int ID;
    public Item item;
    private ItemDatabase itemDatabase;

	// Use this for initialization
	void Start () {
        itemDatabase = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();
        if(item == null)
            item = (Item)itemDatabase.FetchItemById(ID).Clone();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Destroy()
    {
        Destroy(this.gameObject);
    }
}
