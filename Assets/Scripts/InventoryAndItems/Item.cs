﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item
{
    public enum ItemType
    {
        None,
        Armor,
        Weapon,
        Consumable,
        Misc,
        Quest
    }

    public Sprite icon;
    public ItemType Type; // type of an item (Armor, Weapon, Consumable, Misc, Quest)
    public int ItemID; //id of an item
    public string Name;//name of an item
    public string DescriptionSummary; //description of an item
    public int Weight; //weight of an item
    public int Price; //price of an item
   
    public ArmorItem ItemArmor;
    public ConsumableItem ItemConsumable;
    public MiscItem ItemMisc;
    public QuestItem ItemQuest;
    public WeaponItem ItemWeapon;
    
    public Item(ArmorItem itemArmor, ItemType type, string name, string descriptionSummary, int itemID, int weight, int price)
    {
        this.ItemArmor = itemArmor;
        this.Type = type;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
        this.ItemID = itemID;
        this.Weight = weight;
        this.Price = price;        
    }
    public Item(ConsumableItem itemConsumable, ItemType type, string name, string descriptionSummary, int itemID, int weight, int price)
    {
        this.ItemConsumable = itemConsumable;
        this.Type = type;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
        this.ItemID = itemID;
        this.Weight = weight;
        this.Price = price;
    }
    public Item(MiscItem itemMisc, ItemType type, string name, string descriptionSummary, int itemID, int weight, int price)
    {
        this.ItemMisc = itemMisc;
        this.Type = type;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
        this.ItemID = itemID;
        this.Weight = weight;
        this.Price = price;
    }
    public Item(QuestItem itemQuest, ItemType type, string name, string descriptionSummary, int itemID, int weight, int price)
    {
        this.ItemQuest = itemQuest;
        this.Type = type;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
        this.ItemID = itemID;
        this.Weight = weight;
        this.Price = price;
    }
    public Item(WeaponItem itemWeapon, ItemType type, string name, string descriptionSummary, int itemID, int weight, int price)
    {
        this.ItemWeapon = itemWeapon;
        this.Type = type;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
        this.ItemID = itemID;
        this.Weight = weight;
        this.Price = price;
    }

    public object Clone()
    {
        return MemberwiseClone();
    }

}
