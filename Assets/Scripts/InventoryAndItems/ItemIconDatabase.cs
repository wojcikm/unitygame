﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;


public class ItemIconDatabase : MonoBehaviour {

    public List<Sprite> ItemIconList;
    private static DirectoryInfo levelDirectoryPath = new DirectoryInfo(Application.dataPath + "/Resources/ItemIcons");

    // Use this for initialization
    void Awake () {
        

        FileInfo[] fileInfo = levelDirectoryPath.GetFiles("itemIcon*.*");
        for (int i = 0;i< fileInfo.Length;i++)
        {
            if(fileInfo[i].Name.IndexOf(".meta") == -1)
            {
                string[] string1 = fileInfo[i].Name.Split('.');
                ItemIconList.Add(LoadNewSprite(fileInfo[i].FullName, string1[0]));
            }
        }         
    }
    public Sprite LoadNewSprite(string FilePath, string name, float PixelsPerUnit = 100.0f)
    {

        // Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference

        Sprite NewSprite = new Sprite();
        Texture2D SpriteTexture = LoadTexture(FilePath);
        NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), new Vector2(0, 0), PixelsPerUnit);
        NewSprite.name = name;
        return NewSprite;
    }
    public Texture2D LoadTexture(string FilePath)
    {

        // Load a PNG or JPG file from disk to a Texture2D
        // Returns null if load fails

        Texture2D Tex2D;
        byte[] FileData;

        if (File.Exists(FilePath))
        {
            FileData = File.ReadAllBytes(FilePath);
            Tex2D = new Texture2D(2, 2);           // Create new "empty" texture
            if (Tex2D.LoadImage(FileData))           // Load the imagedata into the texture (size is set automatically)
                return Tex2D;                 // If data = readable -> return texture
        }
        return null;                     // Return null if load failed
    }

    // Update is called once per frame

}
