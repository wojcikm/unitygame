﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System;

public class ItemDatabase : MonoBehaviour {

    public List<Item> database;
    private JsonData itemData;
    private List<Sprite> iconDatabase;

    void Awake()
    {
        //database = new List<Item>();
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        ConstructItemDatabase();              
    }
    void Start()
    {
        iconDatabase = GameObject.Find("ItemIconsDatabase").GetComponent<ItemIconDatabase>().ItemIconList;
        for(int i=0;i<iconDatabase.Count;i++)
        {
            int id = Convert.ToInt16(iconDatabase[i].name.Substring(8));
            for(int j=0;j<database.Count;j++)
            {
                if(database[j].ItemID == id)
                {
                    database[j].icon = iconDatabase[i];
                    break;
                }
            }
        }
    }

    void ConstructItemDatabase()
    {
        for(int i = 0;i<itemData.Count;i++)
        {
            if ((Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()) == Item.ItemType.Armor)
            {
                List<string> temporaryList = new List<string>();
                for (int j = 0; j < itemData[i]["effects"].Count; j++)
                {
                    temporaryList.Add((string)itemData[i]["effects"][j]);
                }

                database.Add(new Item(
                    new ArmorItem(
                        (int)itemData[i]["durability"],
                        (int)itemData[i]["defence"],
                        temporaryList,
                        (int)itemData[i]["requiredLevel"],
                        (ArmorItem.ArmorType)Enum.Parse(typeof(ArmorItem.ArmorType), itemData[i]["armorType"].ToString())),
                    (Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()),
                    itemData[i]["name"].ToString(),
                    itemData[i]["descriptionSummary"].ToString(),
                    (int)itemData[i]["itemID"],
                    (int)itemData[i]["weight"],
                    (int)itemData[i]["price"]));
            }
            else if ((Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()) == Item.ItemType.Weapon)
            {
                List<string> temporaryList = new List<string>();

                for (int j = 0; j < itemData[i]["effects"].Count; j++)
                {
                    temporaryList.Add((string)itemData[i]["effects"][j]);
                }
                database.Add(new Item(
                    new WeaponItem(
                        (int)itemData[i]["attackSpeed"],
                        (int)itemData[i]["damage"],
                        temporaryList,
                        (int)itemData[i]["requiredLevel"],
                        (WeaponItem.WeaponType)Enum.Parse(typeof(WeaponItem.WeaponType), itemData[i]["weaponType"].ToString())),
                    (Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()),
                    itemData[i]["name"].ToString(),
                    itemData[i]["descriptionSummary"].ToString(),
                    (int)itemData[i]["itemID"],
                    (int)itemData[i]["weight"],
                    (int)itemData[i]["price"]));
            }
            else if ((Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()) == Item.ItemType.Misc)
            {
                database.Add(new Item(
                    new MiscItem(),
                    (Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()),
                    itemData[i]["name"].ToString(),
                    itemData[i]["descriptionSummary"].ToString(),
                    (int)itemData[i]["itemID"],
                    (int)itemData[i]["weight"],
                    (int)itemData[i]["price"]));

            }
            else if ((Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()) == Item.ItemType.Consumable)
            {
                {
                    database.Add(new Item(
                    new ConsumableItem(
                        (int)itemData[i]["healthRegen"],
                        (int)itemData[i]["manaRegen"],
                        (int)itemData[i]["duration"]),
                    (Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()),
                    itemData[i]["name"].ToString(),
                    itemData[i]["descriptionSummary"].ToString(),
                    (int)itemData[i]["itemID"],
                    (int)itemData[i]["weight"],
                    (int)itemData[i]["price"]));
                }
            }
            else if ((Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()) == Item.ItemType.Quest)
            {
                database.Add(new Item(
                    new QuestItem(
                        (int)itemData[i]["questID"]),
                        
                (Item.ItemType)Enum.Parse(typeof(Item.ItemType), itemData[i]["type"].ToString()),
                    itemData[i]["name"].ToString(),
                    itemData[i]["descriptionSummary"].ToString(),
                    (int)itemData[i]["itemID"],
                    (int)itemData[i]["weight"],
                    (int)itemData[i]["price"]));

            }
        }
    }
    
    public Item FetchItemById(int id)
    {
        for(int i = 0;i<database.Count;i++)
        {
            if (id == database[i].ItemID)
                return database[i];
        }

        return null;
    }
}





