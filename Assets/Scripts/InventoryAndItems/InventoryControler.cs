﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryControler : MonoBehaviour {

    public List<Item> inventory;
    public int Gold;

    private ObjectiveController objectives;
    private ItemDatabase baza;
    
    void Start () {
        baza = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();
        objectives = GameObject.Find("ObjectiveDatabase").GetComponent<ObjectiveController>();
        
    }	

    public int CalculateAmout(Item item)
    {
        int counter = 0;
        foreach(Item inventoryItem in inventory)
        {
            if (inventoryItem == item)
                counter++;
        }
        return counter;
    }

    public List<Item> FilterInventory(Item.ItemType type)
    {
        List<Item> sortedItems = new List<Item>();
        foreach (Item item in inventory)
        {
            if (item.Type == type && !sortedItems.Contains(item))
            {
                sortedItems.Add(item);
            }
        }
        return sortedItems;
    }
    public List<Item> FilterInventory(Item.ItemType type, Item.ItemType type2)
    {
        List<Item> sortedItems = new List<Item>();
        foreach (Item item in inventory)
        {
            if (item.Type == type || item.Type == type2)
            {
                if(!sortedItems.Contains(item))
                    sortedItems.Add(item);
            }
        }
        return sortedItems;
    }

    public void AddItem(int i)
    {
        inventory.Add(baza.database.Find(x => x.ItemID == i));
        objectives.UpdateCollectProgress();
        
    }

    public void AddItem(string name)
    {
        inventory.Add(baza.database.Find(x => x.Name.Contains(name)));
        objectives.UpdateCollectProgress();
    }

    public void RemoveItem(int i)
    {
        inventory.Remove(inventory.Find(x => x.ItemID == i));
        objectives.UpdateCollectProgress();
    }

    public void RemoveItem(string name)
    {
        inventory.Remove(inventory.Find(x => x.Name.Contains(name)));
        objectives.UpdateCollectProgress();
    }

    public void ReceiveGold(int amount)
    {
        Gold += amount;
    }

    public void SpendGold(int amount)
    {
        Gold -= amount;
    }
}
