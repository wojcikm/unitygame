﻿using UnityEngine;
using System.Collections;

public class ConsumableItem {

    public int HealthRegen; //increase of hpregen after consuming
    public int ManaRegen; //increase of manaregen after consuming
    public int Duration;//duration of potion

    public ConsumableItem(int healthRegen, int manaRegen, int duration)
    {
        this.HealthRegen = healthRegen;
        this.ManaRegen = manaRegen;
        this.Duration = duration;    
    }
}
