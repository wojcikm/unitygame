﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponItem{

    public enum WeaponType
    {
        TwoHanded,
        OneHanded,
        Rod,
        Dagger,
        Bow
    }

    public float AttackSpeed; //attack speed of that item
    public int Damage; //damage of an item
    public List<string> Effects; //list of additional effects of an item

    public int RequiredLevel; //level required to wear item
    public WeaponType Class; //type of weapon (TwoHanded, OneHanded, Rod, Dagger, Bow)

    public WeaponItem(float attackSpeed, int damage, List<string> effects,
         int requiredLevel, WeaponType weaponType)
    {
        this.AttackSpeed = attackSpeed;
        this.Damage = damage;
        this.Effects = effects;
        this.RequiredLevel = requiredLevel;
        this.Class = weaponType;
    }
}
