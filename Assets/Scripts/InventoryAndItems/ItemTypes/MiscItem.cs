﻿using UnityEngine;
using System.Collections;

public class MiscItem {

    public bool Stackable;//if item can be stacked in inventory
    public int Amount;//maximum amount of items in the stack

    public MiscItem()
    {        
    }
}
