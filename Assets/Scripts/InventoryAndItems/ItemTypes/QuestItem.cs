﻿using UnityEngine;
using System.Collections;

public class QuestItem {

    public int QuestID; //id of quest that item is required for
    public bool Stackable; //if item can be stacked in inventory
    public int Amount; //maximum amount of items in the stack
    
    public QuestItem(int questID)
    {
        this.QuestID = questID;
        
    }
}
