﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ArmorItem {

    public enum ArmorType
    {
        Head,
        Body,
        Arms,
        Legs,
        Shield
    }

    public int Durability; //current durability of an item
    public int Defence; //armor of an item
    public List<string> Effects; //list of additional effects granted by an item
    
    public int RequiredLevel; //level required to wear 
    public ArmorType Class; //type of an armor (Head, Body, Arms, Legs, Shield)
        
    public ArmorItem(int durability, int defence, List<string> effects, int requiredLevel, ArmorType armorType)
    {
        this.Durability = durability;
        this.Defence = defence;
        this.Effects = effects;
        this.RequiredLevel = requiredLevel;
        this.Class = armorType;
    }
}
