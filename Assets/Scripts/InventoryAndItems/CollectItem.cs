﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectItem : MonoBehaviour {

    private new Camera camera;
    private float maxDistance;
    private ItemScript item;
    private MonsterScript monster;    
    private InventoryControler inventory;
    private PlayerStats player;

	// Use this for initialization
	void Start () {
        maxDistance = 2.0f;
        camera = GetComponentInChildren<Camera>();
        inventory = GameObject.Find("Player").GetComponent<InventoryControler>();
        player = GameObject.Find("Player").GetComponent<PlayerStats>();
    }
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, maxDistance))
            {
                if (hit.collider.tag == "Item")
                {                    
                    item = hit.transform.GetComponent<ItemScript>();
                    inventory.AddItem(item.ID);
                    item.Destroy();                    
                    Debug.Log(inventory.inventory.Count);
                }
                if (hit.collider.tag == "Monster")
                {
                    monster = hit.transform.GetComponent<MonsterScript>();

                    monster.TakeDamage(player.DamageClose);
                    }
            }
        }
    }
}
