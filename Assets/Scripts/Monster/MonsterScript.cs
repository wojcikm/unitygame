﻿using UnityEngine;
using System.Collections;

public class MonsterScript : MonoBehaviour {

    public Monster monster;
    public int level;
    private MonsterDatabase monsterBase;
    private PlayerStats player;
    
    private ObjectiveController objective;

    public int monsterID;

    // Use this for initialization
    void Start() {
        if(level == 0)
        {
            level = 1;
        }
        monsterBase = GameObject.Find("MonsterDatabase").GetComponent<MonsterDatabase>();
        monster = (Monster)monsterBase.FetchMonsterById(monsterID).Clone();
        player = GameObject.Find("Player").GetComponent<PlayerStats>();
        objective = GameObject.Find("ObjectiveDatabase").GetComponent<ObjectiveController>();
        ScaleLevel();
    }

    // Update is called once per frame
    void Update() {
        HealthRegeneration();
    }
    
    private void HealthRegeneration()
    {
        if (monster.CurrentHealth < monster.MaxHealth)
        {
            monster.CurrentHealth += (monster.MaxHealth/180)*Time.deltaTime;
        }
    }

    public void TakeDamage(float damage)
    {
        float Damage = damage * ((100 - monster.Defence) / 100);
        

        if (Damage < 1)
            Damage = 1;
        monster.CurrentHealth = monster.CurrentHealth - Damage;
        Debug.Log("Dealt " + Damage + " to monster named: " + monster.Name);
        if (monster.CurrentHealth <= 0)
        {
            monster.CurrentHealth = 0;
            MonsterDead();
        }
    }
    public void MonsterDead()
    {
        player.GainExperience(monster.KillExpReward);
        DropItems();
        objective.UpdateMonsterKillProgress(monsterID);
        //DestroyObject
        Destroy(gameObject);
    }
    public void DropItems()
    {
        Vector3 monsterPosition = this.transform.position;
        Quaternion monsterRotation = this.transform.rotation;
        
        for (int i = 0; i < monster.KillItemReward.Count; i++)
        {
            string temporaryString = "Item";
            temporaryString = temporaryString + monster.KillItemReward[i].ToString();
            Debug.Log(temporaryString);
            GameObject itemToSpawn = Resources.Load(temporaryString) as GameObject;
            Vector3 monsterDropRandomize = new Vector3(Random.Range(-1, 1), 1, Random.Range(-1, 1));
            Instantiate(itemToSpawn,monsterPosition+monsterDropRandomize,monsterRotation);
        }
        //Instantiate Item drops
    }

    public void ScaleLevel()
    {
        /*
        monster.MaxHealth = monster.MaxHealth + monster.MaxHealth * (level * monster.LevelRatio);
        monster.CurrentHealth = monster.MaxHealth;
        monster.Damage = monster.Damage + monster.Damage * (level * monster.LevelRatio);
        monster.Defence = monster.Defence + monster.Defence * (level * monster.LevelRatio);
        */
    }
}
