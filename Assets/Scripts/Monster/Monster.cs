﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Monster {

    public int MonsterID; //id of a monster
    public string Name; //name of a monster 
    public string DescriptionSummary; //description of a monster
    public int Size; //size of a monster
    public int Level; //current level of a monster
    public int LevelRatio; //scaling damage, health, defence per level
    public int KillExpReward; //experience reward for killing the monster
    public List<int> KillItemReward; //list of items dropped by monster

    public bool Ranged; //if monster attack is ranged or not
    public bool Moveable; //if monster can move or not
    public bool Agressive; //if monster is agressive or not

    public float Speed; //moving speed of a monster 
    public float Damage; //damage of a monster 
    public float AttackSpeed; //attack speed of a monster

    
    public float MaxHealth; //maximum health of a monster
    public float CurrentHealth; //current health of a monster
    public float Defence; //defence of a monster


    public Monster(int monsterID, string name, string descriptionSummary, int size, int level, int levelRatio, int killExpReward, List<int> killIntReward, bool ranged,
        bool movable, bool agressive, int speed, int damage, int attackSpeed, int maxHealth, int defence)
    {
        this.MonsterID = monsterID;
        this.Name = name;
        this.DescriptionSummary = descriptionSummary;
        this.Size = size;
        this.Level = level;
        this.LevelRatio = levelRatio;
        this.KillExpReward = killExpReward;
        this.KillItemReward = killIntReward;
        this.Ranged = ranged;
        this.Moveable = movable;
        this.Agressive = agressive;
        this.Speed = speed;
        this.Damage = damage;
        this.AttackSpeed = attackSpeed;
        this.MaxHealth = maxHealth;
        this.CurrentHealth = maxHealth;
        this.Defence = defence;
    }

    public object Clone()
    {
        
        return MemberwiseClone();
    }

   
}
