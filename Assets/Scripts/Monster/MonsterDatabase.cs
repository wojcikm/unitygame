﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using LitJson;

public class MonsterDatabase : MonoBehaviour {

    public List<Monster> database = new List<Monster>();
    private JsonData monsterData;

    // Use this for initialization
    void Awake()
    {
        monsterData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Monsters.json"));

        ConstructMonsterDatabase();
    }
    
    void ConstructMonsterDatabase()
    {
        for (int i = 0; i < monsterData.Count; i++)
        {
            List<int> temporaryList = new List<int>();

            for (int j = 0; j < monsterData[i]["killItemReward"].Count; j++)
            {
                temporaryList.Add((int)monsterData[i]["killItemReward"][j]);
            }
                

            database.Add(new Monster(
                (int)monsterData[i]["monsterID"],
                monsterData[i]["name"].ToString(),
                monsterData[i]["descriptionSummary"].ToString(),
                (int)monsterData[i]["size"],
                (int)monsterData[i]["level"],
                (int)monsterData[i]["levelRatio"],
                (int)monsterData[i]["killExpReward"],
                temporaryList,
                (bool)monsterData[i]["ranged"],
                (bool)monsterData[i]["movable"],
                (bool)monsterData[i]["agressive"],
                (int)monsterData[i]["speed"],
                (int)monsterData[i]["damage"],
                (int)monsterData[i]["attackSpeed"],
                (int)monsterData[i]["maxHealth"],
                (int)monsterData[i]["defence"]
                ));
        }
    }
    public Monster FetchMonsterById(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (id == database[i].MonsterID)
                return database[i];
        }

        return null;
    }
}
