﻿using UnityEngine;
using System.Collections;

public class Location {
    //world coords
    private Vector3 worldCoord3D;
    
    public Vector3 WorldCoord3D
    {
        get { return WorldCoord3D; }

    }
    
    public Location(Vector3 worldCoord3D)
    {
        this.worldCoord3D = worldCoord3D;

    }

    public bool Compare(Location location)
    {
        if(worldCoord3D != Vector3.zero && location.worldCoord3D == worldCoord3D)
        {
            return true;
        }
        return false;
    }
}
