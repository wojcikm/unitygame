﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//[RequireComponent(Rigidbody)]
public class PlayerControler : MonoBehaviour {

   

    public class MovementStats
    {
        public float currentSpeed;
        public float crouchSpeed = 3.0f;
        public float walkSpeed = 5.0f;
        public float runSpeed = 10.0f;
        public float inAirMoveSpeed = 3.0f;
        public float jumpPower;

        public float currentHealth;
        public float maxHealth = 100.0f;
        public float currentStamina;
        public float maxStamina = 5.0f;

        public float currentHeight;
        public float standingHeight;
        public float crouchingHeight;

        public Vector3 velocity;
    }

    public class MovementBools
    {
        public bool isRunning;
        public bool isGrounded = true;
        public bool isCrouching = false;
    }

    MovementBools movementBools = new MovementBools();
    MovementStats movementStats = new MovementStats();

    
    private float gravity = -20.0f;
    private float maxDistance = 1.05f;

    public bool isPaused;
    public LayerMask ground;

    Rigidbody characterRigidbody;
    Transform characterTransform;

   
    
	// Use this for initialization
	void Start () {
        
        isPaused = false;
        movementStats.jumpPower = 8.0f;
        characterRigidbody = GetComponent<Rigidbody>();
        movementStats.currentStamina = movementStats.maxStamina;
        characterTransform = GetComponent<Transform>();
        movementStats.standingHeight = characterTransform.transform.localScale.y;
        movementStats.crouchingHeight = movementStats.standingHeight / 2;
        //Player = GameObject.Find("Player").GetComponent<PlayerStats>();
        //Cursor.lockState = CursorLockMode.Locked;
    }
	
    	// Update is called once per frame
	void Update ()
    {
        CheckGrounded();        
        SetMovementSpeed();
        UpdateStamina();
        
       
    }
    void FixedUpdate()
    {
        InputMovement();
        TakeInput();
        ApplyGravity(gravity);
    }

    private void SetMovementSpeed()
    {
        if (movementBools.isGrounded && movementBools.isRunning)
            movementStats.currentSpeed = movementStats.runSpeed;
        else if (movementBools.isGrounded && !movementBools.isRunning)
            movementStats.currentSpeed = movementStats.walkSpeed;
        else if (!movementBools.isGrounded && movementBools.isRunning)
            movementStats.currentSpeed = movementStats.walkSpeed;
        else if (!movementBools.isGrounded && !movementBools.isRunning)
            movementStats.currentSpeed = movementStats.inAirMoveSpeed;
    }
    void CheckGrounded()
    {
        if(Physics.Raycast(new Ray(transform.position, Vector3.down), maxDistance, ground))
        {
            movementBools.isGrounded = true;
        }
        else
        {
            movementBools.isGrounded = false;
        }
        //Debug.Log(movementBools.isGrounded);
    }
    void InputMovement()
    {
        if (Input.GetKey(KeyCode.W))
            characterRigidbody.MovePosition(characterRigidbody.position + transform.forward * movementStats.currentSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.S))
            characterRigidbody.MovePosition(characterRigidbody.position - transform.forward * movementStats.currentSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.D))
            characterRigidbody.MovePosition(characterRigidbody.position + transform.right * movementStats.currentSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.A))
            characterRigidbody.MovePosition(characterRigidbody.position - transform.right * movementStats.currentSpeed * Time.deltaTime);
    }
    void TakeInput()
    {
        //Jumping
        if (Input.GetKeyDown(KeyCode.Space) && !movementBools.isCrouching)
        {
            
            Jump();
        }
        //Escape
        if (Input.GetKeyDown("escape"))
            Pause();
            
        //Running
        if (Input.GetKey(KeyCode.LeftShift))
            movementBools.isRunning = true;
        else
            movementBools.isRunning = false;
        // Crouch
        if (Input.GetKeyDown(KeyCode.C))
        {
            Crouch();
        }
    }
    void Pause()
    {
        isPaused = !isPaused;
    }
    bool isAbove()
    {
        if (Physics.Raycast(new Ray(transform.position, transform.up), movementStats.standingHeight + movementStats.crouchingHeight + 0.05f, ground))
        {
            return true;
        }
        else
        {
            return false;
        }
        }
    void Crouch()
    {
        Vector3 zmiana;
        if (!movementBools.isCrouching && movementBools.isGrounded)
        {
            zmiana = new Vector3(0, movementStats.crouchingHeight, 0);
            movementBools.isCrouching = !movementBools.isCrouching;
        }
        else
        {
            if (!isAbove())
            {
                zmiana = new Vector3(0, movementStats.standingHeight, 0);
                movementBools.isCrouching = !movementBools.isCrouching;
            }
            else
                zmiana = new Vector3(0, movementStats.crouchingHeight, 0);
                
        }

        //GetComponentInChildren<Camera>().transform.localPosition = zmiana;
        zmiana += new Vector3(1, 0, 1);
        GetComponent<CapsuleCollider>().transform.localScale = zmiana;

        
    }
    void UpdateStamina()
    {
        if(movementBools.isRunning)
        {
            movementStats.currentStamina -= Time.deltaTime;
            if(movementStats.currentStamina < 0)
            {
                movementStats.currentStamina = 0;
                movementBools.isRunning = false;
            }
        }
        else if(movementStats.currentStamina < movementStats.maxStamina)
        {
            movementStats.currentStamina += Time.deltaTime / 3;
        }
        //Debug.Log(movementStats.currentStamina);
    }
    void Jump()
    {
        if (movementBools.isGrounded && movementStats.currentStamina >= 0.5f)
        {
            movementStats.currentStamina -= 0.5f;
            characterRigidbody.AddForce(0, movementStats.jumpPower, 0, ForceMode.Impulse);
            Debug.Log(movementStats.jumpPower);
        }
    }
    void ApplyGravity(float gravity)
    {
        characterRigidbody.AddForce(0, gravity * Time.fixedDeltaTime, 0,ForceMode.VelocityChange);
    }
    
}
