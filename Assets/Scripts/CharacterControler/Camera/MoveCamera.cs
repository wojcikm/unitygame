﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

    Vector2 mouseLook;
    Vector2 smoothV;
    public float minimumX = -360f;
    public float maximumX = 360f;
    public float minimumY = -60f;
    public float maximumY = 60f;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;
  

    GameObject character;

	// Use this for initialization
	void Start () {
        character = this.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {

        Vector2 md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
        
        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1.0f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1.0f / smoothing);
        mouseLook += smoothV;

        mouseLook.y = Mathf.Clamp(mouseLook.y, minimumY, maximumY);
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
        
	}
}
