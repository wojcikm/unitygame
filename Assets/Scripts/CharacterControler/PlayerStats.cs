﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class PlayerStats : MonoBehaviour
{

    public PlayerEquip playerEquip;
    public ItemDatabase baza;
    public GameObject StatsWindow;

    public float CurrentHealth; //current health of a player
    public float MaxHealth;// max health of a player
    public float CurrentShieldHealth; //current shield hp of a player
    public float MaxShieldHealth; //max shield hp of a player
    public int CurrentShieldFlat; //current points of flat shield (blocks *amount* of attacks)
    public int MaxShieldFlat; //max points of flat shield (blocks *amount* of attacks)
    public float CurrentMana; //current mana of a player
    public float MaxMana; //max mana of a player

    public float HealthPerSecond; //health regeneration per second
    public float ManaPerSecond; //mana regeneration per second 

    public int CurrentLevel; //current level of a player
    public int MaxLevel; //max level to get

    public int CurrentExperience; //current amount of experience
    public int MaxExperience; //max experience to get on actual level

    public int BaseStrenght; //base str
    public int BaseAgility; //base agi
    public int BaseVitality; //base vitality
    public int BaseInteligence; //base inteligence

    public int Strenght; //total amount of str
    public int Agility;//total amount of agility
    public int Vitality;//total amount of vitality
    public int Inteligence;//total amount of inteligence

    public int StatisticPoints; //statistic points (used to increase stats of a player)

    public float DamageClose; //calculated damage in close attacks
    public float DamageRanged; //calculated damage in ranged attacks
    public float DamageMagic; //calculated damage in magic attacks

    public float AttackSpeed; //attack speed

    public float PhysicalArmor; //armor versus physical attacks
    public float MagicArmor;// armor versus magical attacks

    void Start()
    {
        baza = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();
        playerEquip = GameObject.Find("Player").GetComponent<PlayerEquip>();

    }
    void Update()
    {
        playerEquip.SendMessage("ResetAllStats");
        playerEquip.SendMessage("UpdateItemEffects");
        CalculateAllStats();
        GainExperience(10);

    }


    public PlayerStats()
    {
        this.CurrentLevel = 1;
        this.MaxLevel = 10;
        this.CurrentExperience = 0;
        this.MaxExperience = CalculateMaxExpPerLevel();

        this.BaseStrenght = 1;
        this.BaseAgility = 1;//+items
        this.BaseVitality = 1;//+items
        this.BaseInteligence = 1;//+items

        this.StatisticPoints = CalculateStatPointPerLevel();

        this.MaxHealth = CalculateMaxHpPerLevel() + CalculateMaxHpPerVit(Vitality); // +Statistics +Items
        //Debug.Log(CalculateMaxHpPerLevel());
        //Debug.Log(CalculateMaxHpPerVit());
        //Debug.Log(MaxHealth);
        this.CurrentHealth = MaxHealth;

        this.MaxMana = CalculateMaxManaPerLevel() + CalculateMaxManaPerInt(Inteligence); //+Stats +Items
        this.CurrentMana = MaxMana;

        this.CurrentShieldHealth = 0; //+Items
        this.MaxShieldHealth = 0; // +Items
        this.MaxShieldFlat = 0; // +items
        this.CurrentShieldFlat = 0; //+items

        this.HealthPerSecond = CalculateHealthRegenPerLevel() + CalculateHealthRegenPerVit(Vitality); //+Stats +items
        this.ManaPerSecond = CalculateManaRegenPerLevel() + CalculateManaRegenPerInt(Inteligence); //+Stats + items

        this.DamageClose = CalculateDamageClosePerLevel() + CalculateDamageClosePerStr(Strenght) + CalculateDamageCloseFromItems(); //+stats + items
        this.DamageRanged = CalculateDamageRangePerLevel() + CalculateDamageRangedPerAgi(Agility) + CalculateDamageRangedFromItems(); //+stats +items
        this.DamageMagic = CalculateDamageMagicPerLevel() + CalculateDamageMagicPerInt(Inteligence); //+stats + items

        this.AttackSpeed = CalculateAttackSpeedPerAgi(Agility) + CalculateAttackSpeedPerLevel();

        this.PhysicalArmor = CalculateArmorPerLevel() + CalculatePhysicArmorPerVit(Vitality) + CalculatePhysicArmorFromItems();
        this.MagicArmor = CalculateMagicArmorPerInt(Inteligence); //+stats +items

    }

    public void CalculateAllStats()
    {
        this.MaxExperience = CalculateMaxExpPerLevel();

        this.Strenght = BaseStrenght + playerEquip.strBonus;
        this.Agility = BaseAgility + playerEquip.agiBonus;
        this.Vitality = BaseVitality + playerEquip.vitBonus;
        this.Inteligence = BaseInteligence + playerEquip.intBonus;


        this.MaxHealth = (CalculateMaxHpPerLevel() + CalculateMaxHpPerVit(Vitality)) * playerEquip.maxHpRate + playerEquip.maxHp;
        if (CurrentHealth < MaxHealth)
        {
            this.CurrentHealth += HealthRegen();
        }

        this.MaxMana = (CalculateMaxManaPerLevel() + CalculateMaxManaPerInt(Inteligence)) * playerEquip.maxManaRate + playerEquip.maxMana;
        if (CurrentMana < MaxMana)
        {
            this.CurrentMana += ManaRegen();
        }

        this.CurrentShieldHealth = 0; //+Items
        this.MaxShieldHealth = 0; // +Items
        this.MaxShieldFlat = 0; // +items
        this.CurrentShieldFlat = 0; //+items

        this.HealthPerSecond = (CalculateHealthRegenPerLevel() + CalculateHealthRegenPerVit(Vitality)) * playerEquip.hpRegenRate + playerEquip.hpRegen;
        this.ManaPerSecond = (CalculateManaRegenPerLevel() + CalculateManaRegenPerInt(Inteligence)) * playerEquip.manaRegenRate + playerEquip.manaRegen;


        this.DamageClose = (CalculateDamageClosePerLevel() + CalculateDamageClosePerStr(Strenght)) * playerEquip.dmgCloseRate + playerEquip.dmgClose + CalculateDamageCloseFromItems();
        this.DamageRanged = (CalculateDamageRangePerLevel() + CalculateDamageRangedPerAgi(Agility)) * playerEquip.dmgRangedRate + playerEquip.dmgRanged + CalculateDamageRangedFromItems();
        this.DamageMagic = (CalculateDamageMagicPerLevel() + CalculateDamageMagicPerInt(Inteligence)) * playerEquip.dmgMagicRate + playerEquip.dmgMagic;

        this.AttackSpeed = (CalculateAttackSpeedPerAgi(Agility) + CalculateAttackSpeedPerLevel()) * playerEquip.aspdRate + playerEquip.aspd;

        this.PhysicalArmor = (CalculateArmorPerLevel() + CalculatePhysicArmorPerVit(Vitality)) * playerEquip.physicalArmorRate + playerEquip.physicalArmor + CalculatePhysicArmorFromItems();
        this.MagicArmor = (CalculateMagicArmorPerInt(Inteligence)) * playerEquip.magicArmorRate + playerEquip.magicArmor; // +items
    }


    public float CalculateMaxHpPerLevel()
    {
        return (float)(90 + Mathf.Pow(CurrentLevel, 2));
    }
    public float CalculateMaxManaPerLevel()
    {
        return (float)(15 + CurrentLevel * 3);
    }
    public float CalculateHealthRegenPerLevel()
    {
        return (float)(1 + CurrentLevel / 2);
    }
    public float CalculateManaRegenPerLevel()
    {
        return (float)(1 + CurrentLevel / 2);
    }

    public int CalculateMaxExpPerLevel()
    {
        return (int)(1000 + 1000 * Mathf.Pow(CurrentLevel, 2));
    }
    public int CalculateStatCost(int stat)
    {
        return (int)stat;
    }
    public int CalculateStatPointPerLevel()
    {
        return (int)((5 + CurrentLevel % 10) * 2);
    }

    public float CalculateDamageRangePerLevel()
    {
        return (float)(10 + CurrentLevel);
    }
    public float CalculateDamageClosePerLevel()
    {
        return (float)(1 + CurrentLevel * 2);
    }
    public float CalculateDamageMagicPerLevel()
    {
        return (float)(1 + CurrentLevel);
    }
    public float CalculateAttackSpeedPerLevel()
    {
        return (float)(1 + CurrentLevel);
    }
    public float CalculateArmorPerLevel()
    {
        return (float)(1 + CurrentLevel / 2);
    }

    public float CalculateMaxHpPerVit(int stat)
    {
        return (float)(stat * 25);
    }
    public float CalculateMaxManaPerInt(int stat)
    {
        return (float)(stat * 20);
    }
    public float CalculateHealthRegenPerVit(int stat)
    {
        return (float)(1 + (stat / 10));
    }
    public float CalculateManaRegenPerInt(int stat)
    {
        return (float)(1 + (stat / 10));
    }
    public float CalculateDamageRangedPerAgi(int stat)
    {
        return (float)(stat * 15);
    }
    public float CalculateDamageMagicPerInt(int stat)
    {
        return (float)(stat * 20);

    }
    public float CalculateDamageClosePerStr(int stat)
    {
        return (float)(stat * 10);
    }
    public float CalculateAttackSpeedPerAgi(int stat)
    {
        return (float)((100 + stat) / 60);
    }
    public float CalculatePhysicArmorPerVit(int stat)
    {
        return (float)(stat);
    }
    public float CalculateMagicArmorPerInt(int stat)
    {
        return (float)(stat);
    }



    public float CalculatePhysicArmorFromItems()
    {
        int defence = 0;
        try {
            int Armor = playerEquip.Armor;
            defence += baza.FetchItemById(Armor).ItemArmor.Defence; }
        catch (Exception) { }
        try {
            int Gloves = playerEquip.Gloves;
            defence += baza.FetchItemById(Gloves).ItemArmor.Defence; }
        catch (Exception) { }
        try {
            int Shoes = playerEquip.Shoes;
            defence += baza.FetchItemById(Shoes).ItemArmor.Defence; }
        catch (Exception) { }
        try {
            int Shield = playerEquip.Shield;
            defence += baza.FetchItemById(Shield).ItemArmor.Defence; }
        catch (Exception) { }
        return defence;
    }
    public float CalculateAttackSpeedFromItems()
    {


        try
        {
            int Weapon = playerEquip.Weapon;
            if (Weapon != 0)
            {
                return baza.FetchItemById(Weapon).ItemWeapon.AttackSpeed;
            }
            else
                return 0;
        }
        catch (Exception)
        {
            return 0;
        }
        

        
    }
    public float CalculateDamageCloseFromItems()
    {
        float damage = 0;
        
        try
        {
            int Weapon = playerEquip.Weapon;
            if (baza.FetchItemById(Weapon).ItemWeapon.Class != WeaponItem.WeaponType.Bow)
                damage += baza.FetchItemById(Weapon).ItemWeapon.Damage;
        }
        catch (Exception)
        {
            return damage;
        }
        return damage;
    }
    public float CalculateDamageRangedFromItems()
    {
        float damage = 0;
        
        try
        {
            int Weapon = playerEquip.Weapon;
            if (baza.FetchItemById(Weapon).ItemWeapon.Class == WeaponItem.WeaponType.Bow)
                damage += baza.FetchItemById(Weapon).ItemWeapon.Damage;
        }
        catch (Exception)
        {
            return damage;
        }
        return damage;
    }

    public float HealthRegen()
    {
        return HealthPerSecond * Time.deltaTime;
    }
    public float ManaRegen()
    {
        return ManaPerSecond * Time.deltaTime;
    }

    public float CalculateDamage()
    {
        return CalculateDamageCloseFromItems() + CalculateDamageClosePerLevel() + CalculateDamageClosePerStr(Strenght);
    }

    public void GainExperience(int exp)
    {
        CurrentExperience += exp;
        if (CurrentExperience >= MaxExperience)
        {
            int temporary = CurrentExperience - MaxExperience;
            CurrentExperience = temporary;
            LevelUp();
            MaxExperience = CalculateMaxExpPerLevel();
        }
        CalculateAllStats();
    }
    public void LevelUp()
    {
        if (CurrentLevel < MaxLevel)
        {
            CurrentLevel++;
            CalculateAllStats();
            CurrentHealth = MaxHealth;
            CurrentMana = MaxMana;
            CurrentShieldFlat = MaxShieldFlat;
            CurrentShieldHealth = MaxShieldHealth;
            StatisticPoints += CalculateStatPointPerLevel();
            StatsWindow.GetComponent<StatsGUI>().SetValues();
        }
        CalculateAllStats();
    }
    public void TakeDamage(float damage)
    {
        float Damage = damage * ((1000 - PhysicalArmor) / 1000);
        if (CurrentShieldFlat == 0)
        {
            if (CurrentShieldHealth < damage)
            {
                float temporary = Damage - CurrentShieldHealth;
                CurrentHealth -= temporary;
                CurrentShieldHealth = 0;
            }
            else
            {
                CurrentShieldHealth = -Damage;
            }
        }
        else
        {
            CurrentShieldFlat--;
        }
    }

    public void AddStrength()
    {
        if (StatisticPoints >= CalculateStatCost(BaseStrenght))
        {
            StatisticPoints -= CalculateStatCost(BaseStrenght);
            BaseStrenght++;
            
        }
        CalculateAllStats();
    }
    public void AddAgility()
    {
        if (StatisticPoints >= CalculateStatCost(BaseAgility))
        {
            StatisticPoints -= CalculateStatCost(BaseAgility);
            BaseAgility++;
            
        }
        CalculateAllStats();
    }
    public void AddInteligence()
    {
        if (StatisticPoints >= CalculateStatCost(BaseInteligence))
        {
            StatisticPoints -= CalculateStatCost(BaseInteligence);
            BaseInteligence++;
            
        }
        CalculateAllStats();
    }
    public void AddVitality()
    {
        if (StatisticPoints >= CalculateStatCost(BaseVitality))
        {
            
            StatisticPoints -= CalculateStatCost(BaseVitality);
            BaseVitality++;
        }
        CalculateAllStats();
    }

}

