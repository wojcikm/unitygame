﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerEquip : MonoBehaviour {

    private InventoryControler inventory;
    private ItemDatabase baza;

    public int Armor=0;
    public int Helm=0;
    public int Shoes=0;
    public int Shield=0;
    public int Gloves=0;
    public int Weapon=0;

    public float maxHp=0;
    public float maxHpRate=1;
    public float maxMana=0;
    public float maxManaRate=1;
    public float hpRegen=0;
    public float hpRegenRate=1;
    public float manaRegen=0;
    public float manaRegenRate=1;

    public int strBonus=0;
    public int agiBonus=0;
    public int vitBonus=0;
    public int intBonus=0;

    public float dmgClose=0;
    public float dmgCloseRate=1;
    public float dmgRanged=0;
    public float dmgRangedRate=1;
    public float dmgMagic=0;
    public float dmgMagicRate=1;

    public float aspd=0;
    public float aspdRate=1;

    public float physicalArmor=0;
    public float physicalArmorRate=1;
    public float magicArmor=0;
    public float magicArmorRate=1;

    void Awake()
    {
        inventory = GameObject.Find("Player").GetComponent<InventoryControler>();
        baza = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();

    }

    void Update()
    {
        //if (inventory.inventory.Count != 0)
            //EquipItem(1);
        ResetAllStats();
        UpdateItemEffects();
    }

    void ResetAllStats()
    {
         maxHp = 0;
     maxHpRate = 1;
     maxMana = 0;
     maxManaRate = 1;
     hpRegen = 0;
     hpRegenRate = 1;
     manaRegen = 0;
     manaRegenRate = 1;

     strBonus = 0;
     agiBonus = 0;
     vitBonus = 0;
     intBonus = 0;

     dmgClose = 0;
     dmgCloseRate =1;
     dmgRanged = 0;
     dmgRangedRate = 1;
     dmgMagic = 0;
     dmgMagicRate =1;

     aspd = 0;
     aspdRate =1;

     physicalArmor = 0;
     physicalArmorRate = 1;
     magicArmor = 0;
     magicArmorRate =1;
}
    void UpdateItemEffects()
    {
        string statName;
        int statAmount;
        char separator = ',';
        List<int> temporaryList = new List<int>();
        if(Armor!=0)        temporaryList.Add(Armor);
        if(Gloves!=0)        temporaryList.Add(Gloves);
        if (Shoes != 0)            temporaryList.Add(Shoes);
        if (Shield != 0)            temporaryList.Add(Shield);
        if (Weapon != 0)            temporaryList.Add(Weapon);
        for (int i = 0; i < temporaryList.Count; i++)
        {
            if (baza.FetchItemById(temporaryList[i]).Type == Item.ItemType.Armor)
            {
                for (int j = 0; j < baza.FetchItemById(temporaryList[i]).ItemArmor.Effects.Count; j++)
                {
                    string temporary = baza.FetchItemById(temporaryList[i]).ItemArmor.Effects[j];
                    statName = temporary.Split(separator)[0];
                    statAmount = Int32.Parse(temporary.Split(separator)[1]);

                    SetItemUpdates(statName, statAmount);
                }
            }
            else if (baza.FetchItemById(temporaryList[i]).Type == Item.ItemType.Weapon)
                {
                for (int j = 0; j < baza.FetchItemById(temporaryList[i]).ItemWeapon.Effects.Count; j++)
                {
                    string temporary = baza.FetchItemById(temporaryList[i]).ItemWeapon.Effects[j];
                    statName = temporary.Split(separator)[0];
                    statAmount = Int32.Parse(temporary.Split(separator)[1]);

                    SetItemUpdates(statName, statAmount);
                }
            }
        }
    }

    void SetItemUpdates(string statName, int statAmount)
    {
        switch (statName) {
            case "maxHp":
                {
                    maxHp += statAmount;
                    break;
                }
            case "maxHpRate":
                {
                    maxHpRate += 1+statAmount/100;
                    break;
                }
            case "maxMana":
                {
                    maxMana += statAmount;
                    break;
                }
            case "maxManaRate":
                {
                    maxManaRate += 1+statAmount/100;
                    break;
                }
            case "hpRegen":
                {
                    hpRegen += statAmount;
                    break;
                }
            case "hpRegenRate":
                {
                    hpRegenRate += 1+statAmount/100;
                    break;
                }
            case "manaRegen":
                {
                    manaRegen += statAmount;
                    break;
                }
            case "manaRegenRate":
                {
                    manaRegen += 1+statAmount/100;
                    break;
                }
            case "strBonus":
                {
                    strBonus += statAmount;
                    break;
                }
            case "agiBonus":
                {
                    agiBonus += statAmount;
                    break;
                }
            case "vitBonus":
                {
                    vitBonus += statAmount;
                    break;
                }
            case "intBonus":
                {
                    intBonus += statAmount;
                    break;
                }
            case "dmgClose":
                {
                    dmgClose += statAmount;
                    break;
                }
            case "dmgCloseRate":
                {
                    dmgCloseRate += 1+statAmount/100;
                    break;
                }
            case "dmgRanged":
                {
                    dmgRanged += statAmount;
                    break;
                }
            case "dmgRangedRate":
                {
                    dmgRangedRate += 1+statAmount/100;
                    break;
                }
            case "dmgMagic":
                {
                    dmgMagic += statAmount;
                    break;
                }
            case "dmgMagicRate":
                {
                    dmgMagicRate += 1+statAmount/100;
                    break;
                }
            case "aspd":
                {
                    aspd += statAmount;
                    break;
                }
            case "aspdRate":
                {
                    aspdRate += 1+statAmount/100;
                    break;
                }
            case "physicalArmor":
                {
                    physicalArmor += statAmount;
                    break;
                }
            case "physicalArmorRate":
                {
                    physicalArmorRate += 1+ statAmount/100;
                    break;
                }
            case "magicArmor":
                {
                    magicArmor += statAmount;
                    break;
                }
            case "magicArmorRate":
                {
                    magicArmorRate += 1+statAmount/100;
                    break;
                }
            default:
                {

                    break;
                }
        }
    }

    

    public void EquipItem(int id)
    {
        
            if (inventory.inventory.Contains(baza.FetchItemById(id)) && baza.FetchItemById(id).Type == Item.ItemType.Armor)
            {
                switch (baza.FetchItemById(id).ItemArmor.Class)
                {
                    case ArmorItem.ArmorType.Body:
                        UnequipArmor();
                        Armor = baza.FetchItemById(id).ItemID;
                        break;
                    case ArmorItem.ArmorType.Arms:
                        UnequipGloves();
                        Gloves = baza.FetchItemById(id).ItemID;
                        break;
                    case ArmorItem.ArmorType.Head:
                        UnequipHelm();
                        Helm = baza.FetchItemById(id).ItemID;
                        break;
                    case ArmorItem.ArmorType.Legs:
                        UnequipShoes();
                        Shoes = baza.FetchItemById(id).ItemID;
                        break;
                    case ArmorItem.ArmorType.Shield:
                        UnequipShield();
                        Shield = baza.FetchItemById(id).ItemID;
                        break;
                    default:
                        break;
                }



            }
            else if (inventory.inventory.Contains(baza.FetchItemById(id)) && baza.FetchItemById(id).Type == Item.ItemType.Weapon)
            {
                UnequipWeapon();
                Weapon = baza.FetchItemById(id).ItemID;
            }
        
        inventory.RemoveItem(id);
    }

    public void UnequipArmor()
    {
        if (Armor != 0)
        {
            inventory.inventory.Add(baza.database.Find(x => x.ItemID == Armor));
            Armor = 0;
        }
    }
    public void UnequipHelm()
    {
        if (Helm != 0)
        {
            inventory.inventory.Add(baza.database.Find(x => x.ItemID == Helm));
            Helm = 0;
        }
    }
   public void UnequipShoes()
    {
        if (Shoes != 0)
        {
            inventory.inventory.Add(baza.database.Find(x => x.ItemID == Shoes));
            Shoes = 0;
        }
    }
    public void UnequipGloves()
    {
        if (Gloves != 0)
        {
            inventory.inventory.Add(baza.database.Find(x => x.ItemID == Gloves));
            Gloves = 0;
        }
    }
    public void UnequipShield()
    {
        if (Shield != 0)
        {
            inventory.inventory.Add(baza.database.Find(x => x.ItemID == Shield));
            Shield = 0;
        }
    }
    public void UnequipWeapon()
    {
        if (Weapon != 0)
        {
            inventory.inventory.Add(baza.database.Find(x => x.ItemID == Weapon));
            Weapon = 0;
        }
    }    
   
}

