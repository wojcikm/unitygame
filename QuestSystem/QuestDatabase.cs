﻿using UnityEngine;
using System.Collections.Generic;
using QuestSystem;
using System.IO;
using LitJson;
using System;

public class QuestDatabase : MonoBehaviour {

    public List<Quest> QuestBase;
    private JsonData questData;

        // Use this for initialization
    void Start () {
        questData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        ConstructQuestDatabase();
    }

    private void ConstructQuestDatabase()
    {
        for (int i = 0; i < questData.Count; i++)
        {
            QuestBase.Add(new Quest(
                questData[i]["name"].ToString(),
                questData[i]["descriptionSummar"].ToString(),
                questData[i]["dialog"].ToString(),
                questData[i]["hint"].ToString(),


                (int)questData[i]["questID"],
                (int)questData[i]["chainQuestId"],
                (int)questData[i]["sourceID"],

                questData[i]["title"].ToString(),
                questData[i]["description"].ToString(),
                (bool)questData[i]["isBonus"],
                (bool)questData[i]["isComplete"]));
                
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
