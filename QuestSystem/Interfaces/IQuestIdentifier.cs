﻿namespace QuestSystem
{

    public interface IQuestIdentifier
    {
        int QuestID { get; }
        int ChainQuestID { get; }
        int SourceID { get; }


    }
}
