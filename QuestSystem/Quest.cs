﻿using UnityEngine;
using System.Collections.Generic;
namespace QuestSystem
{
    

    public class Quest
    {
        private string name;
        private string descriptionSummary;
        private string dialog;
        private string hint;

        private int questID;
        private int chainQuestID;
        private int sourceID;

        private string description;
        private bool isBonus;
        private bool isComplete;
        private string title;
        
        private List<IQuestObjective> objectives;
        //Name
        //DescriptionSummary
        //QuestHint
        //QuestDialog
        //sourceID
        //questID
        //chainquest and the next quest is blank
        //chainquestID
        private IQuestInformation information;
        public IQuestInformation Information
        {
            get { return information; }
        }
        public Quest(string name, IQuestObjective qo)
        {
            

        }



        //objectives
        
        
        //a) collection objective (10 items, 10 kills)
        //b) location (go from a to b)


        //bonusobjectives
        //rewards

        //events
        //on complete
        //on failed
        //on updateevent
        

        private bool IsComplete()
        {
            for(int i=0; i < objectives.Count;i++)
            {
                if (!objectives[i].IsComplete && !objectives[i].IsBonus)
                {
                    return false;
                }
                    
            }
            return true; 
        }
    }
}


