﻿using System;

namespace QuestSystem
{
    public class QuestInformation : IQuestInformation
    {
        private string name;
        private string descriptionSummary;
        private string dialog;
        private string hint;



        public string DescriptionSummary
        {
            get
            {
                return descriptionSummary;
            }
        }

        public string Dialog
        {
            get
            {
                return dialog;
            }
        }

        public string Hint
        {
            get
            {
                return hint;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
        }
    }
}
//Name
//DescriptionSummary
//QuestHint
//QuestDialog
//sourceID
//questID
//chainquest and the next quest is blank
//chainquestID
//objectives
//bonusobjectives
//rewards
//events
//on complete
//on failed
//on updateevent
//