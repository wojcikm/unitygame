﻿using System;

namespace QuestSystem
{
    public class LocationObjective : IQuestObjective
    {
        private string title;
        private string description;

        private bool isComplete;
        private bool isBonus;

        private Location targetLocation; //3d coord


        public string Title
        {
            get
            {
                return title;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
        }

        public bool IsComplete
        {
            get
            {
                return isComplete;
            }
        }

        public bool IsBonus
        {
            get
            {
                return isBonus;
            }
        }

        public void UpdateProgress()
        {
            if (PlayerControler.GetLocation.Compare(targetLocation))
            {
                isComplete = true;
            }
            else
                isComplete = false;
        }

        public void CheckProgress()
        {
            throw new NotImplementedException();
        }
    }
}
