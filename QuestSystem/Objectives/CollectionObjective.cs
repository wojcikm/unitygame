﻿using UnityEngine;
using System.Collections;
using System;

namespace QuestSystem
{
    public class CollectionObjective : IQuestObjective
    {
        private string title;
        private string description;

        private bool isComplete;
        private bool isBonus;

        private string verb;

        private int collectionAmount;
        private int currentAmount = 0;

        private int itemToCollectID;

        ItemDatabase baza = GameObject.Find("ItemDatabase").GetComponent<ItemDatabase>();

        /// <summary>
        /// This constructor builds a collection objective
        /// </summary>
        /// <param name="titleVerb">Describes the type of collection</param>
        /// <param name="totalAmount">Amount required to complete objective</param>
        /// <param name="item">id of item to be collected</param>
        /// <param name="descrip">describe what will be collected</param>
        /// <param name="bonus">bonus</param>
        public CollectionObjective(string titleVerb, int totalAmount, int item, bool bonus)
        {

            title = titleVerb + " " + totalAmount + " " + baza.database.Find(x => x.ID == item).Title;
            verb = titleVerb;
            description = baza.database.Find(x => x.ID == item).Description;
            itemToCollectID = item;
            collectionAmount = totalAmount;
            currentAmount = 0;
            isBonus = bonus;
            CheckProgress();
        }



        public string Title
        {
            get
            {
                return title;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
        }

        public int CollectionAmount
        {
            get
            {
                return collectionAmount;
            }
        }
        public int CurrentAmount
        {
            get
            {
                return currentAmount;
            }
        }
        public int ItemToCollectID
        {
            get
            {
                return itemToCollectID; 
            }
        }

        public bool IsComplete
        {
            get
            {
                return isComplete;
            }
        }

        public bool IsBonus
        {
            get
            {
                return isBonus;
            }
        }

        public void UpdateProgress()
               {
            throw new NotImplementedException();
        }

        public void CheckProgress()
        {
            if (currentAmount >= collectionAmount)
            { isComplete = true; }
            else
                isComplete = false;
        }

        public override string ToString()
        {
            return currentAmount + "/" + collectionAmount + " " + baza.database.Find(x => x.ID == itemToCollectID).Title + " " + verb + "ed!";
        }
    }
}
