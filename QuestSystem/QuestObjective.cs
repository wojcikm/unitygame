﻿using UnityEngine;
using System.Collections;
using System;

namespace QuestSystem
{
    public class QuestObjective : IQuestObjective
    {
        private string description;
        private bool isBonus;
        private bool isComplete;
        private string title;
        

        public string Description
        {
            get
            {
                return description;
            }
        }

        public bool IsBonus
        {
            get
            {
                return isBonus;
            }
        }

        public bool IsComplete
        {
            get
            {
                return isComplete;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }
        }

        public void CheckProgress()
        {
            throw new NotImplementedException();
        }

        public void UpdateProgress()
        {
            throw new NotImplementedException();
        }
    }
}
